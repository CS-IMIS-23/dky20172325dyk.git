/*
   Coin.java       Author: Dyk

   Representsa coin with two sidse that can be flipped
*/

public class Coin
{
  private final int HEADS = 0;
  private final int TAILS = 1;

  private int face;

  /*
    Sets up the coin by flipping it initially
  */
  public Coin()
  {
    flip();
  }

  /*
     FLips the coin by randomly choosing a face value.
  */
  public void flip()
  {
    face = (int)(Math.random()*2);
  }
 
  /*
     Return ture if the current face of the coin is head.
  */
  public boolean isHeads()
  {
   return (face == HEADS);
  }
  
  /*
     Return the current face of the coin as a string.
  */
  public String tostring()
  {
     String faceName;
     if (face == HEADS)
        faceName = "Heads";
     else
        faceName = "Tails";

      return faceName;
  }
  
}
