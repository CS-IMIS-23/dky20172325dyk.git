/*
   Age.java      Author: Dyk
 
   Demonstrates the use of an if statement
*/

import java.util.Scanner;

public class Age 
{
 /*
   Reads the user'age and prints comment accordingly
 */
 public static void main(String[] ages)
 {
  final int MINOR = 21;

  Scanner scan = new Scanner(System.in);
  System.out.print("Enter your age: ");
  int age = scan.nextInt();

  System.out.println("You entered: "+age);

  if (age < MINOR)
     System.out .println("Youth is wonderful thing. Enjoy.");

  System.out.println("age is a state of mind.");
 }

}
