/*
    CoinFlip.java        Author: Dyk

    Demonstrates the use of an if-else statement
*/

public class CoinFlip
{
 /*
   Creates a Coin object, flip it, and prints the results.
 */
 public static void main(String[] arge)
{
   Coin myCoin = new Coin();
   
   myCoin.flip();

   System.out.println(myCoin);

   if (myCoin.isHeads())
      System.out.println("You win.");
   else
      System.out.println("Better luck next time");
 }

}
