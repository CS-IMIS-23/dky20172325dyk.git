package wek1;

public class LinearNode<T> {
    private LinearNode<T> next;
    private T element;

    //creats an empty node
    public LinearNode (){
        next = null;
        element = null ;
    }

    //creats a node storing the specified element.
    public LinearNode (T elem){
        next = null ;
        element = elem ;
    }

    //returns the node that follows this one
    public LinearNode<T> getNext(){
        return next;
    }

    //Sets the node that follows this one
    public void setNext(LinearNode<T> node ){
        next = node ;
    }

    //returns the element stored in this node
    public T getElement(){
        return element;
    }

    //sets the element stored in this node
    public void setElement (T elem ){
        element = elem ;
    }
}