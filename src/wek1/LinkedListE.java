package wek1;

public class LinkedListE {
    public static void main(String[] args) {
        Student2 student = new Student2("李楠",30,"睡觉");
        Student2 student2 = new Student2("赵晓海",12,"打球");
        Student2 student3 = new Student2("王文彬",29,"打游戏");
        Student2 student4 = new Student2("刘辰",06,"吃");
        Student2 student6 = new Student2("邓煜坤",25,"学习") ;
        Student2 Head = student ;

        InsetNode(Head ,student6 );
        InsetNode(student6 ,student2 );
        InsetNode(student2 ,student4 );
        InsertNode(Head ,student6 ,student2 ,student3 ) ;
        PrintLinked(student );

        System.out.println(" ");

        DeleteNode(Head ,student3 );
        DeleteNode(Head ,student4 );


        PrintLinked(student );
    }
    public static void PrintLinked(Student2 Head){
        Student2 node = Head ;
        while (node !=null){
            System.out.println("姓名："+node .name +"学号："+node .number +"爱好："+node .hobby );
            node = node.next ;
        }
    }
    public static void InsetNode(Student2 Head ,Student2 node){//尾部插入
        Student2 temp=Head;
        while (temp.next!=null){
            temp=temp.next;
        }
        temp.next=node;
        //头插法

//        node.next = Head ;
//        Head = node ;
    }
    public static void InsertNode(Student2 Head,Student2 node1,Student2 node2,Student2 node3){//中间插入
        Student2 point =Head;
        while (point.number != node1.number && point != null ){
            point = point .next;
        }
        if (point.number == node1 .number ){
            node3.next  = point .next ;
            point .next = node3 ;
        }
    }
    public static void DeleteNode(Student2 Head,Student2 node ){//删除示例
        Student2 PreNode = Head ,CurrNode = Head ;
        while (CurrNode != null){
            if (CurrNode.number != node .number ){
                PreNode = CurrNode;
                CurrNode = CurrNode.next;
            }else{
                break;
            }
        }
        PreNode .next = CurrNode .next ;
    }
}