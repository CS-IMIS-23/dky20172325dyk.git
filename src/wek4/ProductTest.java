package wek4;

import wek4.LinkedList.LinkedUnorderedList;


public class ProductTest {
    public static void main(String[] args) {
        LinkedUnorderedList linkedUnorderedList = new LinkedUnorderedList();

        linkedUnorderedList.addToRear(new Product("球鞋" ,999 ,"NIKE"));
        linkedUnorderedList.addToRear(new Product("球裤" ,359 ,"adidas"));
        linkedUnorderedList.addToRear(new Product("紧身坎肩" ,234 , "安德玛"));
        linkedUnorderedList.addToRear(new Product("护膝" ,199 , "NIKE"));
        linkedUnorderedList.addToFront(new Product("口红" ,489 , "TF"));

        System.out.println("商品总数为：");
        System.out.println(linkedUnorderedList.size());
        System.out.println("--------------------------------------------------------");
        System.out.println("最初的列表");
        System.out.println(linkedUnorderedList.toString());
        System.out.println("--------------------------------------------------------");
        System.out.println("商品中是否有球裤？");
        System.out.println(linkedUnorderedList.find("球裤"));
        System.out.println("--------------------------------------------------------");
        System.out.println("排序后商品的列表为：");
        linkedUnorderedList.SelectSorting();
        System.out.println(linkedUnorderedList.toString());
        System.out.println("--------------------------------------------------------");




    }
}