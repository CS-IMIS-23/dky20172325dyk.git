package wek4;

import java.io.IOException;
public class POS2Tester {
    public static void main(String[] args) throws IOException, ElementNotFoundException {
        ProgramOfStudy2 pos = new ProgramOfStudy2();
        System.out.println("判断是否为空："+pos.isEmpty());
        System.out.println("列表长度："+pos.size());

        pos.courseAdd(new Course("CS", 101, "Introduction to Programming", "A-"));
        pos.courseAdd(new Course("ARCH", 305, "Building Analysis", "A"));
        pos.courseAdd(new Course("GER", 210, "Intermediate German"));
        pos.courseAdd(new Course("CS", 320, "Computer Architecture"));
        pos.courseAdd(new Course("THE", 201, "The Theatre Experience"));

        Course ifContain=new Course("CS",101,"Introduction to Programming");
        System.out.println("判断是否有CS 101：Introduction to Programming"+" "+pos.contains(ifContain));

        System.out.println(pos);
        System.out.println("第一个列表元素："+pos.first());
        System.out.println("最后一个列表元素："+pos.last());

        pos.remove(ifContain);
        System.out.println("去掉元素CS 101：Introduction to Programming后的列表："+"\n "+pos.toString());

        pos.removeFirst();
        pos.removeLast();
        System.out.println("去掉头和尾之后的列表："+"\n"+pos.toString());
    }
}