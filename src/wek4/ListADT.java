package wek4;

import java.util.Iterator;


public interface ListADT<T> extends Iterable<T>
{

    public T removeFirst();


    public T removeLast();


    public T remove(T element) throws ElementNotFoundException;


    public T first();


    public T last();


    public boolean contains(T target);


    public boolean isEmpty();


    public int size();


    @Override
    public Iterator<T> iterator();


    @Override
    public String toString();
}