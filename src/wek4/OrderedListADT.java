package wek4;

public interface OrderedListADT<T> extends ListADT<T>
{

    public void add(T element) throws NonComparableElementException;
}