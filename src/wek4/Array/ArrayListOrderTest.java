package wek4.Array;

import wek4.NonComparableElementException;

public class ArrayListOrderTest {
    public static void main(String[] args) throws NonComparableElementException {
        ArrayListOrder arrayListOrder = new ArrayListOrder() ;
        arrayListOrder.add("a");
        arrayListOrder.add("b");
        arrayListOrder.add("c");
        arrayListOrder.add("d");
        arrayListOrder.add("e");

        System.out.println(arrayListOrder.toString()) ;
        System.out.println(arrayListOrder.size()) ;
        System.out.println(arrayListOrder.contains("c"));
        System.out.println(arrayListOrder.first()) ;
        System.out.println(arrayListOrder.last() );
        System.out.println(arrayListOrder.isEmpty()) ;
        arrayListOrder.remove("d");
        System.out.println(arrayListOrder.toString()) ;
        arrayListOrder.removeFirst();
        System.out.println(arrayListOrder.size() );
        System.out.println(arrayListOrder.toString() );

        System.out.println();

        ArrayListUnordered arrayListUnordered = new ArrayListUnordered();
        arrayListUnordered.addToFront("a");
        arrayListUnordered.addToFront("b");
        arrayListUnordered.addToFront("c");
        arrayListUnordered.addToFront("d");
        arrayListUnordered.addToRear("f");
        System.out.println(arrayListUnordered.toString());

        arrayListUnordered.addAfter("e","d");
        System.out.println(arrayListUnordered.toString());
        System.out.println(arrayListUnordered.size());
    }
}
