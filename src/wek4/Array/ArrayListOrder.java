package wek4.Array;

import wek4.NonComparableElementException;
import wek4.OrderedListADT;

public class ArrayListOrder<T> extends ArrayList<T> implements OrderedListADT<T> {
    @Override
    public void add(T elem) throws NonComparableElementException {
        if (!(elem instanceof Comparable)) {
            throw new NonComparableElementException("OrderList");
        }
        Comparable<T> comparableElement = (Comparable<T>) elem;
        if (size() == list.length)
            expandCapacity();

        int scan = 0;
        while (scan < rear && comparableElement.compareTo(list[scan]) > 0)
            scan++;
        for (int shift = rear; shift > scan; shift--)
            list[shift] = list[shift - 1];

        list[scan] = elem;
        rear++;
        modCount++;
    }
}