package wek4;

public class LinearNode<E>
{
    public LinearNode<E> next;
    protected E element;


    public LinearNode()
    {
        next = null;
        element = null;
    }


    public LinearNode(E elem)
    {
        next = null;
        element = elem;
    }

    public LinearNode<E> getNext()
    {
        return next;
    }


    public void setNext(LinearNode<E> node)
    {
        next = node;
    }


    public E getElement()
    {
        return element;
    }

    //填充
    public void setElement(E elem)
    {
        element = elem;
    }

    public int compareTo(E target) {
        String head=String.valueOf(element);
        String temp=String.valueOf(target);
        return head.compareTo(temp);
    }
}
