package wek4;

import wek4.Array.ArrayListOrder;

public class POSTest {
    public static void main(String[] args) throws NonComparableElementException {
        Course course1 = new Course("CS", 101, "Introduction to Programming", "A-");
        Course course2 = new Course("ARCH", 305, "Building Analysis", "A");
        Course course3 = new Course("GER", 210, "Intermediate German");
        Course course4 = new Course("CS", 320, "Computer Architecture");
        Course course5 = new Course("THE", 201, "The Theatre Experience");

        ArrayListOrder arrayListOrder = new ArrayListOrder() ;

        arrayListOrder.add(course1);
        arrayListOrder.add(course2);
        arrayListOrder.add(course3);
        arrayListOrder.add(course4);
        arrayListOrder.add(course5);

        System.out.print(arrayListOrder.toString()+" ");

        System.out.println("第一个和第二个进行比较 ：" + course1.compareTo(course2));
        if(course1.compareTo(course2)==1) {
            System.out.println("course1 : " + course1);
            System.out.println("course2 : " + course2);
        } else {
            System.out.println("course1 : " + course2);
            System.out.println("course2 : " + course1);
        }

        System.out.println("将第三个和第四个进行比较 ：" + course3.compareTo(course4));
        if(course3.compareTo(course4)==1) {
            System.out.println("course3 : " + course3);
            System.out.println("course4 : " + course4);
        } else {
            System.out.println("course1 : " + course3);
            System.out.println("course2 : " + course4);
        }
    }
}