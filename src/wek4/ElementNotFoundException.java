package wek4;

public class ElementNotFoundException extends Throwable {
    public ElementNotFoundException(String arrayList) {

        super ("The element is not in this " + arrayList );
    }
}
