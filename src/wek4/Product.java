package wek4;

import java.io.Serializable;

public class Product implements Serializable,Comparable<Product> {
    private String title;
    private int price;
    private String info;

    public Product(String title, int price, String info) {
        this.title = title;
        this.price = price;
        if (info == null)
            this.info = "";
        else
            this.info = info;
    }

    public Product(String title, int price) {
        this(title, price, "");
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean taken() {
        return !info.equals("");
    }

    @Override
    public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof Product) {
            Product otherProduct = (Product) other;
            if (title.equals(otherProduct.getTitle()))// && price == otherProduct.getPrice()
                result = true;
        }
        return result;
    }


    @Override
    public String toString() {
        String result = "\n" + title + " " + price + ": ";
        if (!info.equals(""))
            result += "  [" + info + "]";
        return result;
    }

    @Override
    public int compareTo(Product product) {
        int result;
        String Producttitle = product.getTitle();
        Integer ProductPrice = product.price;//޸Ĺ
        if (title == Producttitle) {
            result = 0;
        } else {
            result = ((Integer)this.price).compareTo(ProductPrice);
        }
        return result;
    }
}

