package wek4.LinkedList;

import wek4.LinearNode;
import wek4.NonComparableElementException;
import wek4.OrderedListADT;

public class LinkedOrderedList<T> extends LinkedList<T> implements OrderedListADT<T> {

    public LinkedOrderedList() {
        super();
    }

    @Override
    public void add(T element) throws NonComparableElementException {
        if (!(element instanceof Comparable)) {
            throw new NonComparableElementException("LinkedList");
        }
        Comparable<T> comparableElement = (Comparable<T>) element;
        LinearNode node = new LinearNode(comparableElement);
        LinearNode temp = head;
        for (int i = 0; i < count-1; i++)
        {
            temp = temp.getNext();
        }

        if (size() == 0)
        {
            head = node;//列表为空
            count++;
            return;
        }
        else if (comparableElement.compareTo(head.getElement()) < 0)
        {//插入在列表头部
            node.setNext(head);
            head = node;
            count++;
            return;
        }
        else if (comparableElement.compareTo((T) temp.getElement()) > 0)
        {//插入在列表尾部
            temp.setNext(node);
            count++;
        }
        else
        {//插入在列表中间
            LinearNode current = head;
            LinearNode currentnode = current.getNext();
            for (int i = 0; i < count - 1; i++) {
                if (comparableElement.compareTo((T) currentnode.getElement()) < 0) {
                    current.setNext(node);
                    node.setNext(currentnode);
                    count++;
                    return;
                }
                current = currentnode;
                currentnode = currentnode.getNext();
            }
        }
    }
}
