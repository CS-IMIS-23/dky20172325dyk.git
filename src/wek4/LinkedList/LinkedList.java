package wek4.LinkedList;

import wek1.EmptyCollectionException;
import wek4.ElementNotFoundException;
import wek4.LinearNode;
import wek4.ListADT;
import wek4.Product;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class LinkedList<T> implements ListADT<T>, Iterable<T>
{
    protected int count;
    protected LinearNode<T> head;
    protected LinearNode<T> tail;
    protected int modCount;


    public LinkedList()
    {
        count = 0;
        head = tail = null;
        modCount = 0;
    }



    @Override
    public T removeFirst() throws EmptyCollectionException {
        LinearNode node = head;
        if (size() ==1 )
            head = tail =null ;
        else
            head =node.getNext() ;
        count --;
        modCount ++;
        return (T) node.getElement();

    }


    @Override
    public T removeLast() throws EmptyCollectionException {
        LinearNode node = head ;
        T result = (T) node.getElement();
        if(size()==1)
            head = tail =null ;
        else
            while (node.getNext() != tail){
                node = node.getNext() ;//将尾变成尾之前的那个值，之后要将尾部的那个值赋为null，才可以算把它删掉
            }
        tail = node;
        node.setNext(null);
        count --;
        return result ;


    }


    @Override
    public T remove(T targetElement) throws EmptyCollectionException, ElementNotFoundException
    {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = head;

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else
            {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1)  // only one element in the list
            head = tail = null;
        else if (current.equals(head))  // target is at the head
            head = current.getNext();
        else if (current.equals(tail))  // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        }
        else  // target is in the middle
            previous.setNext(current.getNext());

        count--;
        modCount++;

        return current.getElement();
    }


    @Override
    public T first() throws EmptyCollectionException {
        return head .getElement() ;
    }


    @Override
    public T last() throws EmptyCollectionException {
        return tail .getElement() ;

    }


    @Override
    public boolean contains(T targetElement) throws EmptyCollectionException {
        LinearNode node = head ;
        while((node.getElement() != targetElement )&& (node != null) ){
            node = node.getNext();
        }
        return true;

    }


    @Override
    public boolean isEmpty(){
        if(size() == 0)
            return true;
        else
            return false ;
    }


    @Override
    public int size() {
        return count ;
    }


    @Override
    public String toString() {
        LinearNode node = head;
        String result = "";
        for(int a=0;a<count;a++)
        {
            result += node.getElement() +" ";
            node = node.getNext();
        }
        return result;
    }

    public void SelectSorting() {//选择排序法
        LinearNode<Product> min;
        Product temp;
        LinearNode<Product> numNode = (LinearNode<Product>) head;
        while (numNode != null) {
            min = numNode;
            LinearNode<Product> current = min.next;
            while (current != null) {
                if (current.getElement().compareTo(min.getElement()) < 0) {
                    min = current;
                }
                current = current.next;
            }
            temp = min.getElement();
            min.setElement(numNode.getElement());
            numNode.setElement(temp);
            numNode = numNode.next;
        }
    }

    public Product find(String target){
        LinearNode<Product> temp = (LinearNode<Product>) head;
        while(temp !=  null&&!temp.getElement().getTitle() .equals(target)){
            temp = temp.getNext();
        }
        return temp.getElement();
    }


    @Override
    public Iterator<T> iterator()//迭代器
    {
        return new LinkedListIterator();
    }


    private class LinkedListIterator implements Iterator<T>
    {
        private int iteratorModCount;  // the number of elements in the collection
        private LinearNode<T> current;  // the current position


        public LinkedListIterator()
        {
            current = head;
            iteratorModCount = modCount;
        }


        @Override
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (iteratorModCount != modCount)
                throw new ConcurrentModificationException();

            return (current != null);
        }


        @Override
        public T next() throws ConcurrentModificationException
        {
            if (!hasNext())
                throw new NoSuchElementException();

            T result = current.getElement();
            current = current.getNext();
            return result;
        }


        @Override
        public void remove() throws UnsupportedOperationException
        {
            throw new UnsupportedOperationException()  ;
        }
    }

}