package wek4.LinkedList;

import wek4.ElementNotFoundException;
import wek4.LinkedList.LinkedOrderedList;
import wek4.LinkedList.LinkedUnorderedList;
import wek4.NonComparableElementException;

public class LinkedlistTest {
    public static void main(String[] args) throws NonComparableElementException, ElementNotFoundException {
        LinkedOrderedList linkedOrderedList = new LinkedOrderedList();
        linkedOrderedList.add("a");
        linkedOrderedList.add("b");
        linkedOrderedList.add("c");
        linkedOrderedList.add("d");

        System.out.println(linkedOrderedList.toString() );
        System.out.println(linkedOrderedList.size());
        System.out.println(linkedOrderedList.isEmpty() );
        System.out.println(linkedOrderedList.contains("c") );
        System.out.println(linkedOrderedList.first() );
        linkedOrderedList.removeFirst();
        linkedOrderedList.remove("d") ;
        System.out.println(linkedOrderedList.toString() );
        System.out.println(linkedOrderedList.size() );


        System.out.println();


        LinkedUnorderedList linkedUnorderedList =new LinkedUnorderedList();
        linkedUnorderedList.addToFront("e");
        linkedUnorderedList.addToFront("r");
        linkedUnorderedList.addToFront("y");
        linkedUnorderedList.addToFront("u");
        linkedUnorderedList.addToRear("1");
        linkedUnorderedList .addAfter("o","u");

        System.out.println(linkedUnorderedList.size() );
        System.out.println(linkedUnorderedList.toString() );
        linkedUnorderedList.removeFirst() ;
        System.out.println(linkedUnorderedList.toString());
        System.out.println(linkedUnorderedList.size() );

    }
}
