package wek4.LinkedList;

import wek4.LinearNode;
import wek4.Product;
import wek4.UnorderedListADT;

public class LinkedUnorderedList<T> extends LinkedList<T> implements UnorderedListADT<T> {

    public LinkedUnorderedList() {
        super();
    }

    @Override
    public void addToFront(T element) {
        LinearNode node = new LinearNode<T>(element);
        if (head == null) {
            head = node;
            tail = node;
        } else
            node.setNext(head);//node后面接的是之前列表的head，之后将head头变为node
        head = node;
        count++;

    }

    @Override
    public void addToRear(T element) {
        LinearNode node = new LinearNode<T>(element);
        if (head == null) {
            head = node;
            tail = node;
        } else
            tail.setNext(node);//tail的后面那个连接node，node成为列表的尾部
        tail = node;
        count++;
    }

    @Override
    public void addAfter(T element, T target) {
        LinearNode node = new LinearNode(element);
        LinearNode current = head;
        LinearNode temp = current.getNext();
        for (int i = 0; i < count - 1; i++) {
            if (target.equals(current.getElement())) {
                current.setNext(node);
                node.setNext(temp);
                count++;
                return;
            }
            current = temp;
            temp = temp.getNext();
        }
    }


    @Override
    public void SelectSorting() {
        Product product;
        LinearNode<Product> temp1, temp2 = (LinearNode<Product>) head;
        while (temp2 != null) {
            temp1 = temp2;
            LinearNode<Product> current = temp1;
            while (current != null) {
                if (current.getElement().compareTo(temp1.getElement()) < 0) {
                    temp1 = current;
                }
                current = current.getNext();
            }
            product = temp1.getElement();
            temp1.setElement(temp2.getElement());
            temp2.setElement(product);
            temp2 = temp2.getNext();
        }
    }

    public Product find(Product target){
        LinearNode<Product> temp = (LinearNode<Product>) head;
        while(temp !=  null&&!temp.getElement().getTitle() .equals(target)){
            temp = temp.getNext();
        }
        return temp.getElement();
    }
}