package wek4;

import wek1.EmptyCollectionException;
import wek1.LinearNode;
import java.util.Iterator;

public class NewLinkedList<T> implements Iterable<T>, ListADT<T> {

    protected int count;
    protected LinearNode<T> head, tail;
    protected int modCount;
    public NewLinkedList() {
        count = 0;
        head = tail = null;
        modCount = 0;
    }

    @Override
    public T remove(T targetElement) throws EmptyCollectionException, ElementNotFoundException {
        if (isEmpty())
            throw new EmptyCollectionException("NewLinkedList");
        boolean found = false;
        LinearNode<T> previous = null;   // 前一结点
        LinearNode<T> current = head;    // 当前结点
        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            throw new ElementNotFoundException("NewLinkedList");

        if (size() == 1)
            head = tail = null;
        else
        if (current.equals(head))
            head = current.getNext();
        else
        if (current.equals(tail))  {
            tail = previous;
            tail.setNext(null);
        }
        else
            previous.setNext(current.getNext());

        count--;
        modCount++;
        return current.getElement();
    }

    @Override
    public T removeFirst() {
        if (isEmpty())
            throw new EmptyCollectionException("NewLinkedList");
        LinearNode<T> temp = head;
        if (size() == 1)
            head = tail = null;
        else
            head = temp.getNext();

        count--;
        modCount++;
        return temp.getElement();
    }
    @Override
    public T removeLast() {
        if (isEmpty())
            throw new EmptyCollectionException("NewLinkedList");
        boolean found = false;
        LinearNode<T> previous = null;  // 前一结点
        LinearNode<T> temp = head;   // 当前结点

        while (temp != null && !found) {
            if (temp.equals(tail)) {
                found = true;
            } else {
                previous = temp;
                temp = temp.getNext();
            }
        }

        if (size() == 1)
            head = tail = null;
        else {
            tail = previous;
            tail.setNext(null);
        }
        count--;
        modCount++;

        return temp.getElement();
    }

    @Override
    public T first() {
        if (isEmpty())
            throw new EmptyCollectionException("Linkedlist");
        else
            return head.getElement();
    }

    @Override
    public T last() {
        if (isEmpty())
            throw new EmptyCollectionException("Linkedlist");
        else
            return tail.getElement();
    }

    @Override
    public boolean contains(T target) {
        LinearNode temp = head;
        while((temp.getElement() != target)&&(temp !=  null)){
            temp = temp.getNext();
        }
        return temp.getElement() == target;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public String toString() {
        LinearNode<T> temp = head;
        String string = "";
        while(temp != null)
        {
            string += temp.getElement() +"\n";
            temp = temp.getNext();
        }
        return string;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }



}