package week10;

public class Num {
    private NumNode list;

    public Num() {
        list = null;
    }

    public void add(int n) {
        NumNode node = new NumNode(n);
        NumNode current;
        if (list == null) {
            list = node;
        } else {
            current = list;
            while (current.next != null) {
                current = current.next;
            }
            current.next = node;
        }
    }
    public  void selection() {
        NumNode  min;
        int temp;
        NumNode numnode = list;

        while (numnode != null) {
            min = numnode;
            NumNode current = min.next;
            while (current != null) {
                if (current.num  > min.num) {
                    min = current;
                }
                current = current.next;
            }
            temp = min.num ;
            min.num  = numnode .num;
            numnode .num = temp;

            numnode  = numnode .next;
        }
    }

    @Override
    public String toString () {
        String result = "";
        NumNode current = list;
        while (current != null) {
            result += current.num + "\n";
            current = current.next;
        }
        return result;
    }


    private class NumNode {
        public int num;
        public NumNode next;

        public NumNode(int n) {
            num = n;
            next = null;
        }
    }
}