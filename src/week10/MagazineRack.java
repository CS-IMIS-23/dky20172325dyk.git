package week10;

public class MagazineRack {
    public static void main(String[] args) {
        MagazineList rack = new MagazineList();

        rack.add(new Magazine("Time"));
        rack.add(new Magazine("Woodworking Today"));
        rack.add(new Magazine("Communications of the ACM"));
        rack.add(new Magazine("House and Garden"));
        rack.add(new Magazine("GQ"));
        Magazine magazine = new Magazine("Pun");
        rack.add(magazine );
        rack.insert(2, new Magazine("Deng"));
        System.out.println(rack);
        rack.delete(new Magazine("Yuu"), 3);
        System.out.println(rack);
    }
}
