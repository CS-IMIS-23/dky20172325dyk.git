package week10;


//*******************************************************************
//  MagazineList.java       Author: Lewis/Loftus
//
//  Represents a collection of magazines.
//*******************************************************************

public class MagazineList
{
    private MagazineNode list;

    //----------------------------------------------------------------
    //  Sets up an initially empty list of magazines.
    //----------------------------------------------------------------
    public MagazineList()
    {
        list = null;
    }

    //----------------------------------------------------------------
    //  Creates a new MagazineNode object and adds it to the end of
    //  the linked list.
    //----------------------------------------------------------------
    public void add(Magazine mag)
    {
        MagazineNode node = new MagazineNode(mag);
        MagazineNode current;

        if (list == null)
            list = node;
        else
        {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    public void insert(int i, Magazine mag) {
        MagazineNode node = list;
        MagazineNode magazineNode =new MagazineNode(mag );
        if (i != 0){
            int n = 0 ;
            while(n<i - 1){
                node =node.next  ;
                n ++;
            }
            magazineNode .next =node .next ;
            node.next =magazineNode ;
        }
        else
            list =magazineNode  ;
            node.next =magazineNode  ;
    }

        public void delete(Magazine magazine ,int a ){
          int m = 0;
          MagazineNode node = list;
          while(node != null && m<= a -3){
              node = node.next;
              m ++;
            }
          node.next = node.next.next;
    }



    //----------------------------------------------------------------
    //  Returns this list of magazines as a string.
    //----------------------------------------------------------------
    public String toString()
    {
        String result = "";

        MagazineNode current = list;

        while (current != null)
        {
            result += current.magazine + "\n";
            current = current.next;
        }

        return result;
    }

    //*****************************************************************
    //  An inner class that represents a node in the magazine list.
    //  The public variables are accessed by the MagazineList class.
    //*****************************************************************
    private class MagazineNode
    {
        public Magazine magazine;
        public MagazineNode next;

        //--------------------------------------------------------------
        //  Sets up the node
        //--------------------------------------------------------------
        public MagazineNode(Magazine mag)
        {
            magazine = mag;
            next = null;

        }
    }
}