package week10;

import java.util.LinkedList;

public class LinKedListTest{
    public static void main(String[] args) {
        LinkedList<String> lList = new LinkedList<String>();//构造一个Linkedlist.
        lList.add("1");
        lList.add("2");
        lList.add("3");
        lList.add("4");
        lList.add("5");//增添元素


        System.out.println("链表的第一个元素是 : " + lList.getFirst());
        System.out.println("链表最后一个元素是 : " + lList.getLast());
        for (String str: lList) {
            System.out.print(str+" ");
        }
        lList .addFirst("a");
        lList .addLast("d");
        System.out.println();
        System.out.print("链表的元素是"+lList );
        lList .removeFirst();
        lList .remove(3);
        System.out.println();
        System.out.println("链表的元素是"+lList );
        lList .set(2,"hi");
        System.out.println();
        System.out.println("链表的元素是"+lList );
        //System.out.println("链表的元素是："+lList);
    }
}
