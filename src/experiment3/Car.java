package experiment3;


    //
    // this is about the homework PP4.5.
    // And I will make a class.
    //
    public class Car
    {
        private String name;
        private String number;
        private String gd;
        private int date;
        private final int num=2018;
        public Car()
        {
            setName("dazhong");
            setNumber("23456");
            setDate(1999);
        }

        public boolean isAntique()
        { boolean a;
            a= getNum() - getDate() >=45;
            boolean gd = a;
            return gd;          }
        public String getname()
        {  return getName();}
        public String getnumber()
        {   return getNumber();}
        public int getdate()
        {  return getDate();}

        public static void main(String[] args)
        {
            Car car1;
            Car mm = new Car();
            System.out.println("the car's name is:"+mm.getname());
            System.out.println("the car's number is :"+mm.getnumber());
            System.out.println("the car's date is:"+mm.getdate());
            System.out.println("if the car is gudong:"+mm.isAntique());
        }

        @Override
        public String toString() {
            return "Car{" +
                    "name='" + name + '\'' +
                    ", number='" + number + '\'' +
                    ", gd='" + gd + '\'' +
                    ", date=" + date +
                    ", num=" + num +
                    '}';
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getGd() {
            return gd;
        }

        public void setGd(String gd) {
            this.gd = gd;
        }

        public int getDate() {
            return date;
        }

        public void setDate(int date) {
            this.date = date;
        }

        public int getNum() {
            return num;
        }
    }

