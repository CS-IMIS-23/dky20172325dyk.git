package wek10.Haffman;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static wek10.Haffman.HuffmanTree.breadth;


public class HuffmanTest {
    public static void main(String[] args) throws IOException {
        //把字符集从文件中读出来，并保存在一个数组里
        File file = new File("C:\\Users\\DELL\\IdeaProjects\\20172306\\text1.txt");
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String temp = bufferedReader.readLine();

        char characters[] = new char[temp.length()];
        for (int i = 0; i < temp.length(); i++) {
            characters[i] = temp.charAt(i);
        }
        System.out.println("原字符集为：" + Arrays.toString(characters));
        double frequency[] = new double[27];//26个字母加空格
        int numbers = 0;//空格的个数
        for (int i = 0; i < characters.length; i++) {
            if (characters[i] == ' ') {
                numbers++;
            }
            frequency[26] = (float) numbers / characters.length;
        }
        System.out.println("字符集为");
        for (int j = 97; j <= 122; j++) {
            int number = 0;//给字母计数
            for (int m = 0; m < characters.length; m++) {
                if (characters[m] == (char) j) {
                    number++;
                }
                frequency[j - 97] = (float) number / characters.length;
            }
            System.out.print((char) j + "，");
        }
        System.out.println("空格");
        System.out.println("每个字符的概率是" + "\n" + Arrays.toString(frequency));
        double result = 0.0;
        for (int z = 0; z < 27; z++) {
            result += frequency[z];
        }
        System.out.println("总概率之和为" + result);

        List<HuffmanNode> nodes = new ArrayList<HuffmanNode>();
        for (int o = 97; o <= 122; o++) {
            nodes.add(new HuffmanNode((char) o, frequency[o - 97]));
        }
        nodes.add(new HuffmanNode(' ', frequency[26]));

        HuffmanNode root = HuffmanTree.createTree(nodes);

        System.out.println("哈夫曼树为：");

        System.out.println(breadth(root));

        //对英文文件进行编码，输出一个编码后的文件
        String result1 = "";

        List<HuffmanNode> temp1 = breadth(root);

        for (int i = 0; i < characters.length; i++) {
            for (int j = 0; j < temp1.size(); j++) {

                if (characters[i] == temp1.get(j).getData()) {
                    result1 += temp1.get(j).getCode();
                }
            }
        }
        System.out.println("对文件进行编码后的结果为：");
        System.out.println(result1);
        File file2 = new File("C:\\Users\\DELL\\IdeaProjects\\20172306\\text2.txt");
        Writer writer = new FileWriter(file2);
        writer.write(result1);
        writer.close();

        //对英文文件进行解码，输出一个解码后的文件
        List<String> newlist = new ArrayList<>();
        for(int m=0;m < temp1.size();m++)
        {
            if(temp1.get(m).getData()!='无')
                newlist.add(String.valueOf(temp1.get(m).getData()));
        }
        System.out.println("字符："+newlist);

        List<String> newlist1 = new ArrayList<>();
        for(int m=0;m < temp1.size();m++)
        {
            if(temp1.get(m).getData()!='无')
                newlist1.add(String.valueOf(temp1.get(m).getCode()));
        }
        System.out.println("对应编码："+newlist1);



        //先从编完码的文件中读出密文
        FileReader fileReader = new FileReader("C:\\Users\\DELL\\IdeaProjects\\20172306\\text2.txt");
        BufferedReader bufferedReader1 = new BufferedReader(fileReader);
        String secretline = bufferedReader1.readLine();

        //将读出的密文存在secretText列表中
        List<String> secretText = new ArrayList<String>();
        for (int i = 0; i < secretline.length(); i++) {
            secretText.add(secretline.charAt(i) + "");
        }

        //解密
        String result2 = "";//最后的解码结果
        String current="";// 临时的保存值
        while(secretText.size()>0) {
            current = current + "" + secretText.get(0);
            secretText.remove(0);
            for (int p = 0; p < newlist1.size(); p++) {
                if (current.equals(newlist1.get(p))) {
                    result2 = result2 + "" + newlist.get(p);
                    current="";
                }

            }
        }
        System.out.println("解码后的结果："+result2);
        File file3 = new File("C:\\Users\\DELL\\IdeaProjects\\20172306\\text3.txt");
        Writer writer1 = new FileWriter(file3);
        writer1.write(result2);
        writer.close();
    }
}
