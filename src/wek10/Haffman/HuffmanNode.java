package wek10.Haffman;

public class HuffmanNode<T> implements Comparable<HuffmanNode<T>> {
    private char data;
    private double weight;
    private HuffmanNode left;
    private HuffmanNode right;
    String code;

    public HuffmanNode(char data, double weight){
        this.data = data;
        this.weight = weight;
        this.code ="";
    }

    public char  getData() {
        return data;
    }

    public void setData(char data) {
        this.data = data;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public HuffmanNode getLeft() {
        return left;
    }

    public void setLeft(HuffmanNode left) {
        this.left = left;
    }

    public HuffmanNode getRight() {
        return right;
    }

    public void setRight(HuffmanNode right) {
        this.right = right;
    }
    public String getCode(){
        return code;
    }

    public void setCode(String number){
        code = number;
    }
    @Override
    public String toString(){
        return "data:"+this.data+" weight:"+this.weight+" code:"+this.code+"\n\t";
    }

    @Override
    public int compareTo(HuffmanNode node) {
        if(node.getWeight() > this.getWeight()){
            return 1;
        }
        if(node.getWeight() < this.getWeight()){
            return -1;
        }
        else
            return 0;
    }
}


