package wek10.Haffman;
import java.util.*;


public class HuffmanTree<T> {
    public static  HuffmanNode createTree(List<HuffmanNode> nodes) {
        // 只要nodes数组中有2个以上的节点
        while (nodes.size() > 1) {
            Collections.sort(nodes);
            HuffmanNode left = nodes.get(nodes.size() - 1);
            HuffmanNode right = nodes.get(nodes.size() - 2);
            HuffmanNode parent = new HuffmanNode('无', left.getWeight() + right.getWeight());
            parent.setLeft(left);
            left.setCode("0");
            parent.setRight(right);
            right.setCode("1");
            //删除权值最小的两个节点
            nodes.remove(left);
            nodes.remove(right);
            //将新节点加入到集合中
            nodes.add(parent);
        }
        return nodes.get(0);
    }

    public static List<HuffmanNode> breadth(HuffmanNode root) {
        List<HuffmanNode> list = new ArrayList<HuffmanNode>();
        Queue<HuffmanNode> queue = new ArrayDeque<HuffmanNode>();

        if (root != null) {
            queue.offer(root);
            root.getLeft().setCode(root.getCode() + "0");
            root.getRight().setCode(root.getCode() + "1");
        }

        while (!queue.isEmpty()) {
            list.add(queue.peek());
            HuffmanNode node = queue.poll();

            //如果左子节点不为null，将它加入到队列
            if (node.getLeft() != null) {
                queue.offer(node.getLeft());
                node.getLeft().setCode(node.getCode() + "0");
            }
            //如果右子节点不为null，将它加入到队列
            if (node.getRight() != null) {
                queue.offer(node.getRight());
                node.getRight().setCode(node.getCode() + "1");
            }
        }
        return list;
    }
}