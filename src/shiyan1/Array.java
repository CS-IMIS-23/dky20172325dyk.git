package shiyan1;

public class Array {
    protected int nDengYuKun;
    protected  String[] NewArray;

    public Array(String in){
        NewArray =in.split("\\s");
        nDengYuKun = NewArray.length;
    }

    //将一个String数组打印出来
    @Override
    public String toString(){
        String result="";
        for (int a = 0; a< NewArray.length; a++){
            result+= NewArray[a]+" ";
        }
        return result;
    }

    //在一个数组的指定位置（从0开始）插入一个节点，并返回新得到的数组
    public void whereInsert(int where,String num){
        String []result=new String[NewArray.length+1];

        if (where==0){
            result[0]=num;
            for (int a = 0; a< NewArray.length; a++)
            {
                result[a+1]= NewArray[a];
            }
        }
        else
        {
            if (where== NewArray.length){
                for (int a = 0; a< NewArray.length; a++){
                    result[a]= NewArray[a];
                }
                result[NewArray.length+1]=num;
            }

            else {
                result[where]=num;
                for (int a = 0; a< NewArray.length; a++){
                    if (a<where)
                        result[a]= NewArray[a];
                    else
                        result[a+1]= NewArray[a];
                }
            }
        }
        NewArray =result;
        nDengYuKun= NewArray.length;
    }

    //删除数组指定位置（从0开始）的一个节点，并返回新得到的数组
    protected void  whereDelete(int where){
        String []result=new String[NewArray.length-1];

        if (where==0){
            for (int a = 0; a< NewArray.length-1; a++)
            {
                result[a]= NewArray[a+1];
            }
        }
        else
        {
            if (where== NewArray.length-1){
                for (int a = 0; a< NewArray.length-1; a++){
                    result[a]= NewArray[a];
                }
            }

            else {

                for (int a = 0; a< NewArray.length-1; a++){
                    if (a<where)
                        result[a]= NewArray[a];
                    else
                        result[a]= NewArray[a+1];
                }
            }
        }
        NewArray =result;
        nDengYuKun= NewArray.length;

    }

    //返回数组长度nDengYuKun
    public int size()
    {
        return nDengYuKun;
    }


    //显示选择排序的过程
    public String SelectSorting(){
        String result="";
        int[]array=new int[nDengYuKun];

        for (int a=0;a<nDengYuKun;a++ ){
            array[a]=Integer.parseInt(NewArray[a]);
        }
        //升序排序
        for(int i=0;i<array.length-1;i++){
            //升序排列
            for(int j=i+1;j<array.length;j++){
                if(array[i]<array[j]){
                    int min=array[i];
                    array[i]=array[j];
                    array[j]=min;
                }
                String every="";
                for (int a=0;a<array.length;a++) {
                    every += array[a] + " ";
                }
                result+="元素的总数:"+array.length+"当前链表所有元素："+every+"\n";
            }
        }
        return result;
    }

}




