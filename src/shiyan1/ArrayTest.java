package shiyan1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class ArrayTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in );
        System.out.println("输入一些整数，每两个整数之间用空格隔开：");
        String in = scanner.nextLine();

        Array array = new Array(in );
        System.out.println("打印数组："+array.toString());
        System.out.println("数组长度："+array.size());

        String fromFile = readString("C:\\Users\\lenovo\\IdeaProjects\\dky20172325dyk\\src\\shiyan1\\shiyan2Text");

        String[] trans2 = fromFile.split("\\s");

        array.whereInsert(5, trans2[0]);
        System.out.println("打印第5位插入文件中读入的1后的数组：" + array.toString());
        System.out.println("数组长度:" + array.size());

        array.whereInsert(0, trans2[1]);
        System.out.println("打印第0位插入文件中读入的2后的数组:" + array.toString());
        System.out.println("数组长度:" + array.size());

        array.whereDelete(6);
        System.out.println("打印数组中删除刚才的数字1后的数组：" + array.toString());
        System.out.println("数组长度：" + array.size());

        System.out.println("先是选择排序的过程：");
        System.out.println(array.SelectSorting());
    }


    //将文件中的文本保存到字符串的方法
    protected static String readString(String filedizhi) {
        String string = "";
        File file = new File(filedizhi);
        try {
            FileInputStream in = new FileInputStream(file);
            int size = in.available();
            byte[] buffer = new byte[size];
            in.read(buffer);
            in.close();
            string = new String(buffer, "GB2312");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return string;
    }
}
