package shiyan1;

public class NewLinkedList {

    protected NewLinearNode head;
    protected int nDengYuKun;

    public NewLinkedList(){
        head=null;
        nDengYuKun=0;
    }

    //尾插法
    public void Push(int t) {

        NewLinearNode node = new NewLinearNode(t);
        NewLinearNode temp;

        if (head == null) {
            head = node;
        }
        else {
            temp = head;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = node;
        }
        nDengYuKun++;
    }

    //打印链表内容
    @Override
    public String toString() {
        String result = "";
        NewLinearNode temp = head;
        while(temp != null){
            result += temp.getElement()+ " ";
            temp = temp.next;
        }
        return result;

    }

    //返回链表长度
    public int size()
    {
        return nDengYuKun;
    }

    //指定位置插入指定整数
    public void WhereInsert(int where, int t) {
        NewLinearNode node = new NewLinearNode(t);
        NewLinearNode temp1, temp2;

        if (nDengYuKun + 1 >= where) {

            if (where == 1) {
                node.next = head;
                head = node;

            } else {
                temp1 = head;
                temp2 = head.next;
                for (int a = 1; a < where - 1; a++) {
                    temp1 = temp1.next;
                    temp2 = temp2.next;
                }
                if (temp2 != null) {
                    node.next = temp2;
                    temp1.next = node;
                } else if (temp2 == null) {
                    temp1.next = node;
                    }
                }
            nDengYuKun++;
        }
    }
    //删除指定位置指定元素
    public void whereDelete(int where, int num) {
        NewLinearNode node = new NewLinearNode(num);
        NewLinearNode temp1, temp2;
        if((where == 1)&&(head.element==num)){
            head = head.next;
        }
        else{
            if(where <= nDengYuKun + 1){
                temp1 = head;
                temp2 = head.next;
                for(int a = 1; a < where - 1; a++)
                {
                    temp1 = temp1.next;
                    temp2 = temp2.next;
                }
                if(temp2.element==node.element){
                    if(temp2.next != null){
                        temp1.next = temp2.next;
                    }

                    else{
                        temp1.next = null;
                    }
                }
            }
        }
        nDengYuKun--;
    }


    //显示冒泡排序过程
    public String BubbleSort() {
        String [] strings=this.toString().split("\\s");
        int [] out=new int[strings.length];
        for (int a=0;a<strings.length;a++){
            out[a]= Integer.parseInt(strings[a]);
        }
        String result="";
        for (int i=0;i<out.length-1;i++){
            //外层循环控制排序趟数
            for (int j=0;j<out.length-1-i;j++){
                //内层循环控制每一趟排序多少次
                if (out[j]>out[j+1]){
                    int temp=out[j];
                    out[j]=out[j+1];
                    out[j+1]=temp;
                }
                String every="";
                for (int a=0;a<out.length;a++){
                    every+=out[a]+" ";
                }
                result+="元素的总数:"+out.length+"当前链表所有元素："+every+"\n";
            }
        }
        return result;
    }




}
