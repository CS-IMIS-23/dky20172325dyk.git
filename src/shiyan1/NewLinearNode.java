package shiyan1;

public class NewLinearNode {

    protected NewLinearNode next;
    protected int element;

    // 创建空指针
    public NewLinearNode()
    {
        next = null;
        element= Integer.parseInt(null);
    }

    // 创建指针元素
    public NewLinearNode (int elem)
    {
        next = null;
        element = elem;
    }

    // 返回指针next
    public NewLinearNode getNext()
    {
        return next;
    }

    // 更改指针next
    public void setNext (NewLinearNode node)
    {
        next = node;
    }

    // 得到头指针
    public  int getElement()
    {
        return element;
    }

    // 更改头指针
    public void setElement (int elem)
    {
        element = elem;
    }
}


