package shiyan1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class LinkedListTest {
    public static void main(String[] args) {
        String string;
        Scanner scanner =new Scanner(System.in);
        System.out.println("请输入若干整数，每两个整数之间一个空格符隔开：");
        string=scanner.nextLine();

        NewLinkedList newLinkedList=new NewLinkedList();
        String [] a=string.split("\\s");

        for (int i=0;i<a.length;i++){
            newLinkedList.Push(Integer.parseInt(a[i]));
        }
        System.out.println("打印链表："+newLinkedList.toString());
        System.out.println("链表长度："+newLinkedList.size());

        String formFile=readText("C:\\Users\\lenovo\\IdeaProjects\\dky20172325dyk\\src\\shiyan1\\shiyan2Text");
        String []str=formFile.split("\\s");

        newLinkedList.WhereInsert(6,Integer.parseInt(str[0]));
        System.out.println("打印第五位插入文件读入的1后的链表："+newLinkedList.toString());
        System.out.println("链表长度："+newLinkedList.size());

        newLinkedList.WhereInsert(1,Integer.parseInt(str[1]));
        System.out.println("打印第0位插入文件读入的2后的链表："+newLinkedList.toString());
        System.out.println("链表长度："+newLinkedList.size());

        newLinkedList.whereDelete(7,1);
        System.out.println("打印删除刚才插入的数字1后的链表："+newLinkedList.toString());
        System.out.println("链表长度："+newLinkedList.size());

        System.out.println("打印冒泡排序过程：");
        System.out.println(newLinkedList.BubbleSort());

        }

    //将文件中的文本保存到字符串的方法
    protected static String readText(String filedizhi) {
        String string = "";
        File file = new File(filedizhi);
        try {
            FileInputStream in = new FileInputStream(file);
            int size = in.available();
            byte[] buffer = new byte[size];
            in.read(buffer);
            in.close();
            string = new String(buffer, "GB2312");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return string;
    }

}
