//*************************************************************************************************************
//   Conculate.java     Author:DYK
// 用于计算两点之间的距离。
//*************************************************************************************************************
  import java.util.Scanner;

  public class PP35
{
   public static void main(String[] args)
{
   Scanner scan =new Scanner(System.in);
   
   double a,b,c,d,e,f,g,h,i,j;
   System.out.println("请输入点1的横坐标:");
   a = scan.nextDouble();

   System.out.println("请输入点1的纵坐标:");
   b = scan.nextDouble();

   System.out.println("请输入点2的横坐标:");
   c = scan.nextDouble();

   System.out.println("请输入点2的纵坐标:");
   d = scan.nextDouble();

   e = (a-c);
   f = (b-d);

   g =Math.pow(e, 2);
   h =Math.pow(f, 2);

   i =(g+h);
   j =Math.sqrt(i);

   System.out.println("两点间的距离是: " + j);
}
}   
