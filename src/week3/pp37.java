import java.util.Scanner;
import java.text.DecimalFormat;

public class pp37
{
    public static void main(String[] args)
    {
        double A, B, C, D, E, F;

        Scanner scan = new Scanner(System.in);
        DecimalFormat fmt = new DecimalFormat("0.###");
          
        System.out.println("请分别输入三角形的边长: ");
        A = scan.nextDouble();
        
        B = scan.nextDouble();
    
        C = scan.nextDouble();

        D = ((A + B + C) * 0.5);
        E = (D * (D - A) * (D - B) * (D - C));

        F = Math.pow(E, 0.5);
        
        System.out.println("该三角形的面积为: " + fmt.format(F));
    }
}

