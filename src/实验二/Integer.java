package 实验二;

// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=100;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Byte extends  Data {
    int value;

    Byte() {
        value = 20172325%6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
class Short extends  Data {
    int value;

    Short() {
        value = 20172325 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
class Boolean extends  Data {
    int value;

    Boolean() {
        value = 20172325 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
class Long extends  Data {
    int value;

    Long () {
        value = 20172325 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
class Float extends  Data {
    int value;

    Float () {
        value = 20172325 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
class Double extends  Data {
    int value;

    Double() {
        value = 20172325 % 6;
    }

    public void DisplayValue() {
        System.out.println(value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class ByteFactory extends Factory {
    public Data CreateDataObject(){
        return new Byte() ;
    }
}
class ShortFactory extends Factory {
    public Data CreateDataObject(){
        return new Short ();
    }
}
class BooleanFactory extends Factory {
    public Data CreateDataObject(){
        return new Boolean ();
    }
}
class LongFactory  extends Factory {
    public Data CreateDataObject(){
        return new Long ();
    }
}
class FloatFactory extends Factory {
    public Data CreateDataObject(){
        return new Float ();
    }
}
class DoubleFactory extends Factory {
    public Data CreateDataObject(){
        return new Double ();
    }
}

//Client classes
class Document {
    Data pd;
    Document(ShortFactory shortFactory ){
        pd = new Short() ;
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new ShortFactory() );
        d.DisplayData();
    }
}
