package 实验二;

import org.apache.xmlbeans.impl.xb.xsdschema.Public;

import static java.lang.Math.round;
import static java.lang.Math.sqrt;

public class Complex {
    private double R;
    private double I;
    public Complex (double R,double I){
        this.R = R ;
        this.I = I ;
    }

    public double getR() {
        return R;
    }

    public double getI() {
        return I;
    }

    public boolean equals(Complex a){
        boolean result =false ;

        if (R ==a.R && I ==a.I )
            result = true;
        return result ;
    }
    public Complex ComplexAdd(Complex a){
        double r = a.getR() + this.R;
        double i = a.getI()+this.I;
        return new Complex(r ,i );
    }
    public Complex ComplexSub(Complex a){
        double t =this.R -a.getR() ;
        double u =this.I -a.getI() ;
        return new Complex(t,u );
    }
    public Complex ComplexMulti(Complex a){
        double x=R *a.R -I *a.I ;
        double y=R *a.I +I*a.R ;
        return new Complex(x,y);
    }
    public Complex ComplexDiv(Complex a){
        double d = sqrt(a.R*a.R)+sqrt(a.I*a.I);
        double e = (R *a.I -I*a.R );
        R = (R *a.R +I *a.I )/d ;
        I =round(e/d);
        return new Complex(R ,I);
    }
    public String toString(){
        String result="";
        if (I ==0.0)
            result = R +"";
        else
        if (R == 0.0)
            result =I +"i";
        else
        if (I >0.0)
            result = R +""+"+"+I +"i";
        else
            result = R +""+I +"i";
        return result;
    }
}
