package Arithmetic;

/*
 *    后缀表达式的计算     作者：邓煜坤，刘辰
 *
 *    邓煜坤负责的部分
 */

import java.util.Stack;
import java.util.StringTokenizer;


public class HZ {

    /*
     *由于我们的Test文件还没编好，各部分还没有联系起来，所以我先在这里把四个符号做出了声明
     */

    private final char ADD = '+';
    private final char SUBTRACT = '-';
    private final char MULTIPLY = '*';
    private final char DIVIDE = '/';

    private Stack<String> stack;

    /*
     * 初始化一个空栈，用来存储表达式
     */
    public HZ (){
        stack = new Stack<String>();
    }

    /*
     * 此处用来判断指定的char是否为操作符。
     *  如果是运算符，布尔值为真
     */
    private boolean isOperator(String str) {

        return (str.equals("+") || str.equals("-") || str.equals("*") || str
                .equals("÷"));
    }

    /*
     * 下面是具体的计算过程
     */
    private RationalNumber calculateSingleOp(char operator, RationalNumber num1, RationalNumber num2) {

        RationalNumber result = null;
        switch (operator)
        {
            case ADD:
                result = num2.add(num1);
                break;
            case SUBTRACT:
                result = num2.subtract(num1);
                break;
            case MULTIPLY:
                result = num2.multiply(num1);
                break;
            case DIVIDE:
                result = num2.divide(num1);
                break;
        }

        return result;
    }

    /*
     *下面是计算指定的后缀表达式。
     *(1).如果遇到操作数，将其推送到栈上。
     *(2).如果遇到操作符，则弹出两个操作数以供运算符计算。
     *(3).将计算结果推到栈上。
     *字符串表示后缀表达式。
     */
    public String evaluate (String[] str) {

        RationalNumber num1, num2;
        String a = "", result = "";
        for (String s : str) {
            a += s + " ";
        }
        // 指定特定的字符分隔符为空格。
        String token = "";
        StringTokenizer tokenizer = new StringTokenizer(a);

        while (tokenizer.hasMoreTokens()) {
            // 每一个字符都来自于字符串。
            token = tokenizer.nextToken();

            if (isOperator(token)) {
                num1 = become(stack.pop());
                num2 = become(stack.pop());
                result = calculateSingleOp(token.charAt(0), num1, num2).toString();
                // 将计算结果推到栈上
                stack.push(result+"");
            } else {
                stack.push(token);
            }
        }
        return stack.pop();
    }

    private RationalNumber become (String s){
        StringTokenizer A = new StringTokenizer(s,"/");
        int num1 = Integer.parseInt(A.nextToken());
        int num2;
        if(A.hasMoreTokens())
            num2 = Integer.parseInt(A.nextToken());
        else num2 = 1;
        RationalNumber number = new RationalNumber(num1,num2);
        return number;
    }


}