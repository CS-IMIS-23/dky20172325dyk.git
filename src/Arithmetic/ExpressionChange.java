package Arithmetic;


import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/*    中缀转后缀
 *
 *    作者:  邓煜坤、刘辰
 *
 *   本部分负责人：邓煜坤
 */
public class ExpressionChange {

    public boolean comparePrior(String a , String b) {
        if ("*".equals(a ) || "÷".equals(a )) {
            if ("+".equals(b ) || "-".equals(b )) {
                return true;
            }
        }
        return false;
    }

    /*
       转为后缀表达式:
       比较扫描到的运算符，和stack栈顶的运算符。
       如果扫描到的运算符优先级高于栈顶运算符则，把运算符压入栈。
       否则的话，就依次把栈中运算符弹出加到数组newExpressionStrs的末尾，直到遇到优先级低于扫描到的运算符或栈空，并且把扫描到的运算符压入栈中。
       就这样依次扫描，知道结束为止。如果扫描结束，栈中还有元素，则依次弹出加到数组newExpressionStrs的末尾，就得到了后缀表达式。
     */
    public String[] toSuffixExpression(ArrayList<String> getquestion) {
        //        //新组成的表达式
        //String[] expressionStrs = new String[.size()];
        ArrayList q = getquestion;
        String[] expressionStrs = new String[q.size()];
        int index = 0;
        //String[] expressionStrs = new String[question.size()];
        while (index < q.size()) {
            expressionStrs[index] = (String) q.get(index);
            index++;
        }
        List<String> newExpressionStrs = new ArrayList<String>();
        Stack<String> stack = new Stack<String>();
        for (int i = 0; i < expressionStrs.length; i++) {
            if ("+".equals(expressionStrs[i]) || "-".equals(expressionStrs[i]) || "*".equals(expressionStrs[i]) || "÷".equals(expressionStrs[i])) {
                if (!stack.empty()) { // 取出先入栈的运算符
                    String s = stack.pop();
                    if (comparePrior(expressionStrs[i], s)) { //如果栈值优先级小于要入栈的值,则继续压入栈
                        stack.push(s);
                    } else {  //否则取出值
                        newExpressionStrs.add(s);
                    }
                }
                stack.push(expressionStrs[i]);
            } else {
                newExpressionStrs.add(expressionStrs[i]);
            }
        }
        while (!stack.empty()) {
            String s = stack.pop();
            newExpressionStrs.add(s);
        }

        // return newExpressionStrs .toArray() ;

        return  newExpressionStrs.toArray(new String[0]);

    }
}
