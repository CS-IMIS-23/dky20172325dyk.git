package Arithmetic;


//作者：刘辰、邓煜坤
// ProduceQuestion.java 类
// 用以生成题目

import java.util.ArrayList;
import java.util.Random;

public class ProduceQuestion {
    public static void main(String[] args) {

    }



    private String number;//数字
    private String symbol;//符号
    Random liu = new Random();//选择符号
    String[] symbo = {"+", "-", "*", "÷"};

    public String getNumber() {//得到数字
//        int a = liu.nextInt(19) + 1;
//        number = a;
//        return number;
        int a = liu.nextInt(19) + 1;
        int b = liu.nextInt(19) + 1;
        RationalNumber rationalNumber = new RationalNumber(a, b);
        number = rationalNumber.toString();
        return number;
    }

    public String getSymbol() {//得到随机的操作符
        int b = liu.nextInt(4);

        return symbo[b];
    }
    public ArrayList<String> getquestion(int n) {
        ArrayList<String> deng = new ArrayList<>();
        for (int a = 0; a < n; a++) {
            if (a % 2 == 0 || a == 0) {//属于偶数
                String y = getNumber();
                deng.add(String.valueOf(y));
                int m = a / 2;
            }
            if (a % 2 != 0) {//属于奇数
                int b = liu.nextInt(4);
                deng.add(symbo[b]);
                int m = (a - 1) / 2;
            }

        }
        if (n % 2 == 0) {
            String m = getNumber();
            deng.add(String.valueOf(m));
        }
        ArrayList<String> question = deng;
        return question;
    }
}

