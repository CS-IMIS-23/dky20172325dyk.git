package wek7;

import wek6.BinaryTreeADT;
/**
 * @author lc
 * @version
 * @description 类说明
 */
public interface BinarySearchTreeADT<T> extends BinaryTreeADT<T> {

    //添加自身具有的方法
    /**
     * 往树中添加一个元素
     * @param element
     */
    public void addElement(T element);
    /**
     * 树中删除一个元素
     * @param targetElement
     */
    public T removeElement(T targetElement);
    /**
     * 往树中删除元素的任何存在
     */
    public void removeAllOccurrences(T targetElement);
    /**
     * 删除最小的元素
     */
    public T removeMin();
    /**
     * 删除最大的元素
     */
    public T removeMax();
    /**
     */
    public T findMin();
    /**
     * 返回一个指向树中最大元素的引用
     */
    public T findMax();

}
