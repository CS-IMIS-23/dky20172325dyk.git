package wek7;

import wek1.EmptyCollectionException;
import wek4.Array.ArrayListUnordered;
import wek4.ElementNotFoundException;
import wek4.NonComparableElementException;
import wek4.UnorderedListADT;

public class AVLTree<T extends Comparable<T>> {
    protected AVLTreeNode<T> root;

    public AVLTree() {
        root = null;
    }

    public AVLTree(T element) {
        if (!(element instanceof Comparable))
            try {
                throw new NonComparableElementException("AVLTreeNode");
            } catch (NonComparableElementException e) {
                e.printStackTrace();
            }

        root = new AVLTreeNode<T>(element);
    }

    public int height() {
        return height(root);
    }
    private int height(AVLTreeNode<T> tree) {
        if (tree != null)
            return tree.height;

        return 0;
    }

    //查找元素
    public T find(T targetElement) {
        AVLTreeNode<T> current = findAgain(targetElement, root);

        if (current == null)
            try {
                throw new ElementNotFoundException("AVLTree");
            } catch (ElementNotFoundException e) {
                e.printStackTrace();
            }

        return (current.getElement());
    }
    private AVLTreeNode<T> findAgain(T targetElement, AVLTreeNode<T> next) {
        if (next == null)
            return null;
        if (next.getElement().equals(targetElement))
            return next;
        AVLTreeNode<T> temp = findAgain(targetElement, next.getLeft());
        if (temp == null)
            temp = findAgain(targetElement, next.getRight());

        return temp;
    }

    //判断是否为空
    public boolean isEmpty() {
        return (root == null);
    }

    //找寻最小值
    public T findMin(AVLTreeNode<T> tree) {
        T result ;

        if (tree.getElement() == null)
            throw new EmptyCollectionException("AVLTree");
        else {
            if (tree.getLeft() == null) {
                result = tree.getElement();
            } else {
                AVLTreeNode<T> current = tree.getLeft();
                while (current.getLeft() != null) {
                    current = current.getLeft();
                }
                result = current.getElement();
            }
        }
        return result;
    }

    //找寻最大值
    public T findMax(AVLTreeNode<T> tree) {
        T result ;

        if (tree.getElement() == null)
            throw new EmptyCollectionException("AVLTree");
        else {
            if (tree.getRight() == null) {
                result = tree.getElement();
            } else {
                AVLTreeNode<T> current = tree.getRight();
                while (current.getRight() != null) {
                    current = current.getRight();
                }
                result = current.getElement();
            }
        }
        return result;
    }

    //左旋
    private AVLTreeNode<T> leftLeftSpin(AVLTreeNode<T> node) {
        AVLTreeNode<T> temp;//附加的结点

        temp = node.left;//依旧书中的三段
        node.left = temp.right;
        temp.right = node;

        node.height = Math.max(height(node.left), height(node.right)) + 1;
        temp.height = Math.max( height(temp.left), node.height) + 1;//因为平衡改变了，所以高度发生了变化

        return temp;
    }

    //右旋
    private AVLTreeNode<T> rightRightSpin(AVLTreeNode<T> node) {
        AVLTreeNode<T> temp;

        temp = node.right;
        node.right = temp.left;
        temp.left = node;

        node.height = Math.max( height(node.left), height(node.right)) + 1;
        temp.height = Math.max( height(temp.right), node.height) + 1;

        return temp;
    }

    //左右旋
    private AVLTreeNode<T> leftRightSpin(AVLTreeNode<T> node) {
        node.left = rightRightSpin(node.left);//先右旋

        return leftLeftSpin(node);//再左旋
    }

    //右左旋
    private AVLTreeNode<T> rightLeftSpin(AVLTreeNode<T> node) {
        node.right = leftLeftSpin(node.right);//先左旋

        return rightRightSpin(node);//再右旋
    }


    public void addElement(T node) {
        root = addElement(root, node);
    }
    private AVLTreeNode<T> addElement(AVLTreeNode<T> tree, T element) {

        if (!(element instanceof Comparable)) {
            try {
                throw new NonComparableElementException("AVLTreeNode");
            } catch (NonComparableElementException e) {
                e.printStackTrace();
            }
        }

        if (tree == null) {
            tree = new AVLTreeNode<T>(element, null, null);
            if (tree==null) {
                throw new EmptyCollectionException("EmptyCollectionException");
            }
        } else {

            if (element.compareTo(tree.getElement()) < 0) {    // 应该插入到"tree的左子树"的情况
                tree.left = addElement(tree.left, element);
                if (height(tree.right) - height(tree.left) == -2) {//看失衡了，进行调节
                    if (element.compareTo(tree.left.getElement()) < 0)
                        tree = leftLeftSpin(tree);
                    else
                        tree = leftRightSpin(tree);
                }
            } else if (element.compareTo(tree.getElement()) >= 0) {    // 应该插入到"tree的右子树"的情况
                tree.right = addElement(tree.right, element);
                if (height(tree.left) - height(tree.right) == -2) {
                    if (element.compareTo(tree.right.getElement()) > 0)
                        tree = rightRightSpin(tree);
                    else
                        tree = rightLeftSpin(tree);
                }
            }
        }

        tree.height = Math.max( height(tree.left), height(tree.right)) + 1;//高度改变了

        return tree;
    }

    //删除全部元素
    public void removeAllElement(){
        AVLTreeNode<T> root = null;
        this.root = root;
    }

    //删除结点
    public void removeElement(T node) {
        AVLTreeNode<T> z;//要插入的结点

        if ((z = findAgain(node,root)) != null)
            root = removeElement(root, z);
    }
    private AVLTreeNode<T> removeElement(AVLTreeNode<T> tree, AVLTreeNode<T> node) {
        // 根为空 或者 没有要删除的节点，直接返回null。
        if (tree==null || node ==null)
            return null;

        if (node.element.compareTo(tree.element) < 0) {
            tree.setLeft(removeElement(tree.getLeft(), node));// 待删除的节点在左子树
            if (height(tree.getRight()) - height(tree.getLeft()) == 2) {
                AVLTreeNode<T> r =  tree.getRight();
                if (height(r.getLeft()) > height(r.getRight()))
                    tree = rightLeftSpin(tree);
                else
                    tree = rightRightSpin(tree);
            }
        } else if (node.element.compareTo(tree.element) > 0) {
            tree.setRight(removeElement(tree.getRight(), node));// 待删除的节点在右子树
            if (height(tree.getLeft()) - height(tree.getRight()) == 2) {
                AVLTreeNode<T> l =  tree.getLeft();
                if (height(l.getRight()) > height(l.getLeft()))
                    tree = leftRightSpin(tree);
                else
                    tree = leftLeftSpin(tree);
            }
        } else {
            if ((tree.getLeft() != null) && (tree.getRight() != null)) {
                if (height(tree.getLeft()) > height(tree.getRight())) {
                    AVLTreeNode<T> max = (AVLTreeNode<T>)findMax(tree.getLeft());
                    tree.element = max.element;
                    tree.setLeft(removeElement(tree.getLeft(), max));
                } else {
                    AVLTreeNode<T> min = (AVLTreeNode<T>)findMax(tree.getRight());
                    tree.element = min.element;
                    tree.setRight(removeElement(tree.getRight(), min));
                }
            } else {
                tree = (tree.getLeft()!=null) ? tree.getLeft() : tree.getRight();
            }
        }

        return tree;
    }


    //和第十章的printTree一样就是输出树型
    public String toString(){
        UnorderedListADT<AVLTreeNode<T>> nodes = new ArrayListUnordered<AVLTreeNode<T>>();
        UnorderedListADT<Integer> levelList = new ArrayListUnordered<Integer>();

        AVLTreeNode<T> current;
        String result = "";
        int printDepth = this.height();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            } else {
                for (int i = 0; i < ((Math.pow(2,
                        (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }
        }
        return result;
    }


    public static void main(String[] args) {
        AVLTree avlTree = new AVLTree();
        avlTree.addElement(1);
        avlTree.addElement(2);
        avlTree.addElement(3);
        avlTree.addElement(4);
        avlTree.addElement(5);

        System.out.println("输出的树为："+avlTree.toString());
    }
}