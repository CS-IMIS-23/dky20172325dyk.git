package wek7;

import java.util.Iterator;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {
        LinkedBinaryTree num1 = new LinkedBinaryTree("2");
        LinkedBinaryTree num2 = new LinkedBinaryTree("0");
        LinkedBinaryTree num3 = new LinkedBinaryTree("1");
        LinkedBinaryTree num4 = new LinkedBinaryTree("7",num1,num3);
        LinkedBinaryTree num5 = new LinkedBinaryTree("23",num2,num4);
        LinkedBinaryTree num6 = new LinkedBinaryTree("25",num4,num5);

        Iterator it;
        System.out.println("7的右结点为：");
        System.out.println(num4.getRight());
        System.out.println("是否存在元素1?");
        System.out.println(num3.contains("1"));

        System.out.println("前序遍历结果为： ");
        num6 .toPreString() ;

        System.out.println();

        System.out.println("后序遍历结果为：");
        num6 .toPostString() ;

        System.out.println(num6.toString());
    }
}
