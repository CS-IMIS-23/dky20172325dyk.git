package wek7;

import wek6.ExpressionTree;
import wek6.ExpressionTreeOp;

import java.util.Scanner;
import java.util.Stack;

public class PrefixToPostfix{

    //输入一个中缀表达式，使用树将中缀表达式转换为后缀表达式
    private Stack<ExpressionTree> treeExpression;//操作数栈
    private Stack ope;//操作符栈

    public PrefixToPostfix(){
        treeExpression = new Stack<ExpressionTree>();
        ope = new Stack();
    }

    private ExpressionTree getOperand(Stack<ExpressionTree> treeExpression){
        ExpressionTree temp;
        temp = treeExpression.pop();
        return temp;
    }

    public ExpressionTree getPostfixTree(String expression)
    {
        //得到二叉树
        ExpressionTree operand1,operand2;
        char operator;
        String tempToken;

        Scanner parser = new Scanner(expression);

        while(parser.hasNext()){
            tempToken = parser.next();
            operator=tempToken.charAt(0);

            if ((operator == '+') || (operator == '-') || (operator=='*') ||
                    (operator == '/')){
                if (ope.empty())
                    ope.push(tempToken);//当储存符号的栈为空时，直接进栈
                else{
                    String a =ope.peek()+"";//转化为String型的数据才能使用equals方法进行判断，否则永远都是false
                    if (((a.equals("+"))||(a.equals("-")))&&((operator=='*')||(operator=='/')))
                        ope.push(tempToken);//当得到的符号的优先级大于栈顶元素时，直接进栈
                    else {
                        String s = String.valueOf(ope.pop());
                        char temp = s.charAt(0);
                        operand1 = getOperand(treeExpression);
                        operand2 = getOperand(treeExpression);
                        treeExpression.push(new ExpressionTree
                                (new ExpressionTreeOp(1, temp, 0), operand2, operand1));
                        ope.push(operator);
                    }//当得到的符号的优先级小于栈顶元素或者优先级相同时时，数字栈出来两个运算数，形成新的树进栈
                }
            }
            else
                treeExpression.push(new ExpressionTree(new ExpressionTreeOp
                        (2,' ',Integer.parseInt(tempToken)), null, null));
        }
        while(!ope.empty()){
            String a = String.valueOf(ope.pop());
            operator = a.charAt(0);
            operand1 = getOperand(treeExpression);
            operand2 = getOperand(treeExpression);
            treeExpression.push(new ExpressionTree
                    (new ExpressionTreeOp(1, operator, 0), operand2, operand1));
        }
        return treeExpression.peek();
    }

    public String getTree()
    {
        return (treeExpression.peek()).printTree();
    }

    public int getResult(){
        return treeExpression.peek().evaluateTree();
    }
    public void PostOrder() {

       // treeExpression.peek().
//        Iterator =  treeExpression.peek().toPostString() ;
//        String result="";
//        for (;iterator.hasNext();)
//            result +=iterator.next()+" ";
    }

    public static void main(String[] args) {
        String again="";
        Scanner scan = new Scanner(System.in);
        PrefixToPostfix  list = new PrefixToPostfix();

        do{
            System.out.println("请输入一个中缀表达式：");
            String exp = scan.nextLine();
            list.getPostfixTree(exp) ;
            System.out.println(list.getTree());
            System.out.println("后缀表达式为：");
            list.PostOrder() ;
            System.out.println("计算结果为：");
            System.out.println(list.getResult());
            System.out.println("你是否要继续？");
            again=scan.nextLine();
        }
        while (again.equalsIgnoreCase("y"));

    }

}
