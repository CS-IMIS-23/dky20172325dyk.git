package wek7;

public class AVLTreeNode<T extends Comparable<T>> {
    protected T element;
    protected AVLTreeNode<T> left, right;
    protected int height;

    public AVLTreeNode(T obj) {
        this.element = obj;
        this.left = null;
        this.right = null;
        this.height = 0;
    }

    public AVLTreeNode(T element, AVLTreeNode<T> left, AVLTreeNode<T> right) {
        this.element = element;
        this.left = left;
        this.right = right;
        this.height = 0;
    }

    public T getElement() {
        return element;
    }

    public AVLTreeNode<T> getRight() {
        return right;
    }

    public AVLTreeNode<T> getLeft() {
        return left;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public void setLeft(AVLTreeNode<T> left) {
        this.left = left;
    }

    public void setRight(AVLTreeNode<T> right) {
        this.right = right;
    }

    public boolean judge(){
        if(left == null && right == null)
            return false;
        else
            return true;
    }
}