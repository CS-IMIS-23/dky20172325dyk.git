package wek7;


import wek4.ListADT;
import wek4.OrderedListADT;

import java.util.Iterator;

/**
 * BinarySearchTreeList represents an ordered list implemented using a binary
 * search tree.
 *
 */
public class BinarySearchTreeList<T> extends LinkedBinarySearchTree<T> implements ListADT<T>, OrderedListADT<T>, Iterable<T> {
    /**
     * Creates an empty BinarySearchTreeList.
     */
    public BinarySearchTreeList() {
        super();
    }

    /**
     * Adds the given element to this list.
     *
     * @param element the element to be added to the list
     */
    public void add(T element) {
        addElement(element);
    }

    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element in the list
     */
    public T removeFirst() {
        return removeMin();
    }

    /**
     * Removes and returns the last element from this list.
     *
     * @return the last element from the list
     */
    public T removeLast() {
        return removeLast();
    }

    @Override
    public T remove(T element){
        return removeElement(element);
    }

    @Override
    public T first() {
        return findMin();
    }

    @Override
    public T last() {
        return findMax();
    }

    @Override
    public Iterator<T> iterator() {
        return iteratorInOrder();
    }
}