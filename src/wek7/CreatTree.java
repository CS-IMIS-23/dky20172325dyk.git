package wek7;

import java.util.Scanner;
import java.util.StringTokenizer;

public class CreatTree{
    public LinkedBinaryTree<String> CreatTree(String inorder[],String preorder[]){//inorder是中序遍历；preorder是前序遍历
        LinkedBinaryTree<String> binaryTree = null;
        if(inorder.length == preorder.length && inorder.length != 0 && preorder.length != 0){
            int n = 0;//num为索引值
            while (!inorder[n].equals(preorder[0])) {//找root，是前序遍历的第一个数字，中序遍历中的某个数字
                n++;
            }
            String[] preLeft = new String[n];//root的左子树是一个含有num个数的数组
            String[] inLeft = new String[n];//root的左子树是一个含有num个数的数组
            String[] preRight = new String[preorder.length - n - 1];//root的右子树是一个含有length-num-1个数的数组
            String[] inRight = new String[inorder.length - n - 1];//root的右子树是一个含有length-num-1个数的数组
            for (int t = 0; t < inorder.length; t++) {
                if (t < n) {
                    preLeft[t] = preorder[t + 1];//前序遍历的num的左面的都进入preLeft
                    inLeft[t] = inorder[t];//中序遍历的num的左面的都进入inLeft
                }
                if (t > n) {
                    preRight[t - n - 1] = preorder[t];//前序遍历的num的右面都进入preright
                    inRight[t - n - 1] = inorder[t];//中序遍历的num的右面都进入inRight
                }
                if(t == n){//
                    continue;
                }
            }
            //然后将以上重新生成的数组想象成一个新的树，就重新进行以上的过程
            LinkedBinaryTree<String> left = CreatTree(inLeft, preLeft);
            LinkedBinaryTree<String> right = CreatTree(inRight, preRight);
            binaryTree = new LinkedBinaryTree<String>(preorder[0], left, right);//输出树，root和左子树和右子树
        }else {//不满足以上的条件
            binaryTree = new LinkedBinaryTree<>();
        }//那么就是无效的，没办法输出树
        return binaryTree;
    }

    public static void main(String args[]){
        String a,b;
        int i = 0,j = 0;
        Scanner scanner  = new Scanner(System.in);
        System.out.println("请输入树的前序输出：");
        a = scanner.nextLine();
        System.out.println("请输入树的中序输出：");
        b = scanner.nextLine();

        StringTokenizer stringTokenizer1 = new StringTokenizer(a, " ");
        StringTokenizer  stringTokenizer2= new StringTokenizer(b, " ");

        String[] string1 = new String[stringTokenizer1.countTokens()];
        String[] string2 = new String[stringTokenizer2.countTokens()];

        while (stringTokenizer1.hasMoreTokens())
        {
            string1[i] = stringTokenizer1.nextToken();
            i++;
        }//遍历，输出输入的前序
        while (stringTokenizer2.hasMoreTokens())
        {
            string2[j] = stringTokenizer2.nextToken();
            j++;
        }//遍历，输出输入的中序
        CreatTree creattree = new CreatTree();
        LinkedBinaryTree<String> binaryTree = creattree.CreatTree(string2,string1);
        System.out.println(binaryTree.toString());
    }
}

