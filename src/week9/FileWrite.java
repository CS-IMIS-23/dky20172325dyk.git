package week9;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FileWrite {
        public static void main(String[] args) throws IOException {
            File file = new File("C:\\Users\\lenovo\\Desktop", "sort.txt");
                //
                //使用try-catch处理异常
                //
                try {
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                } catch (IOException Exception) {
                    System.out.println("错误，指定路径不存在");
                }

                Scanner num = new Scanner(System.in);
                System.out.println("请输入任何几个整数：");
                String N = num.nextLine();
                BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                /*
                   在这里插入用户想要的数据
                 */
                writer.write(N);
                writer.flush();
                /*
                    这一步将刚才输入的数据读取到文件中
                 */
                InputStreamReader reader = new InputStreamReader(new FileInputStream(file));
                BufferedReader bufferedReader = new BufferedReader(reader);
                String breakpoint = "";
                System.out.print("您想要排序的几个数字是：");
                List result = new LinkedList();
                while (breakpoint != null) {
                    breakpoint = bufferedReader.readLine();
                    if (breakpoint == null)
                        break;
                    else {

                        String q[] = breakpoint.trim().split(" ");
                        for (int i = 0; i < q.length; i++) {
                            int num1 = Integer.valueOf(q[i]);
                            result.add(num1);
                            System.out.print(q[i] + " ");
                        }
                    }
                }
                Integer[] X = (Integer[]) result.toArray(new Integer[0]);
                sort(X);
                System.out.print("排序的结果是：");
                for (Integer r : X) {
                    String c = r + " ";
                    writer.write(c);
                    System.out.print(r + " ");
                }
                writer.flush();

                String t = ""+X ;
                /*
                   写入排序成功的数据
                 */
                writer.write(t);
            }

        /*
            利用插入法排序
        */
        public static void sort(Comparable[] list)
        {
            int min;
            Comparable temp;

            for (int index = 0; index < list.length-1; index++)
            {
                min = index;
                for (int scan = index+1; scan < list.length; scan++)
                    if (list[scan].compareTo(list[min]) < 0)
                        min = scan;
                temp = list[min];
                list[min] = list[index];
                list[index] = temp;
            }
        }
    }

