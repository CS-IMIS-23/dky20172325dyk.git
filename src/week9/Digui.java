package week9;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Digui {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入一个整数n：");

        int n;
        n = scanner.nextInt();

        int result = F(n);
        System.out.println("F(n)的计算结果为：" + result);
        File file =new File("C:\\Users\\lenovo\\Desktop\\文档","递归");
        if(!file.exists())
            file.createNewFile();
        FileWriter writer = new FileWriter(file );
        writer.write("结果是" + result);
        writer.flush();

    }

    static int  F(int n) {
        int result;
        if (n == 0)
            result = 0;
        else
            if (n == 1)
                result = 1;
            else
                result = F(n - 1) + F(n - 2);

        return result;
    }


}

