package week9;

public class StringTooLongException extends Throwable {
    StringTooLongException (String message)
    {
        super(message );
    }
}
