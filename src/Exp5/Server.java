package Exp5;

import java.io.*;
import java.net.*;

public class Server {
    public static void main(String args[]) {
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            serverForClient = new ServerSocket(2008);
        } catch (IOException e1) {
            System.out.println(e1);
        }
        try {
            System.out.println("等待宝宝呼叫");
            socketOnServer = serverForClient.accept(); //堵塞状态，除非有客户呼叫
            System.out.println("宝宝已连接");
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());
            String s = in.readUTF(); // in读取信息，堵塞状态
            System.out.println("大霸王收到宝宝的要求:计算后缀表达式" + s);
            out.writeUTF(MyDC.jisuan(new StringBuffer(s))+"");
        } catch (Exception e) {
            System.out.println("宝宝已断开" + e);
        }
    }
}