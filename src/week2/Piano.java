// pianoKeys.java     Author:Lewis/Lofus
//
//Deminstrates the declaration,initialization, and use of an
//integer variable.
//
  public class Piano
{
/* 
prints the number of key on a piano.
*/
public static void main(String[] args)
{
int keys = 88;
System.out.println("\n\tA piano has\n\t"
                    + keys+"\n\tkeys");
}
}
