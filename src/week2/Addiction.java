//-----------------------------------------------------------------------------
//  Addiction.java             Author:Lewis/Loftus
//
//  Demonstrates the difference between the addiction and string
//  concatenation operators.
//-----------------------------------------------------------------------------

public class Addiction

  {
    //-----------------------------------------------------------------
    //    Concatenates and diference between the addition and string
    //-----------------------------------------------------------------
     
      public static void main(String[] args)
        {
            System.out.println("24 and 45 concatenated: "+24+45);
            
            System.out.println("24 and 45 added: "+(24+45)); 
        }
  }
