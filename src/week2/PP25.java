//
// This is the homework about PP2.5.
//
  import java.util.Scanner;
  public class PP25
  {
  public static void main(String[] args)
  {
   final int BASE = 32;
   final double CONVERSION_FACTOR = 5.0/9.0;
   double celsiusTemp;
   double fahrenheitTemp;
   Scanner scan = new Scanner(System.in);
   System.out.println("the fahrenheitTemp is:");
   fahrenheitTemp = scan.nextDouble();
   celsiusTemp = CONVERSION_FACTOR*(fahrenheitTemp-BASE);
   System.out.println("the celsiusTemp is:"+ celsiusTemp);
         }
 }
