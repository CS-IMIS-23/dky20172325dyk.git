package wek8;

import wek7.BinaryTreeADT;

public interface BiGHeapADT<T> extends BinaryTreeADT<T> {
    public void addElement(T obj);

    public T removeMax();

    public T findMax();
}
