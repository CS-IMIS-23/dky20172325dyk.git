package wek8;

public class ATreeTest {
    public static void main(String[] args) {
        ATree aTree = new ATree();
        int[] list = {36,30,18,40,32,45,22,50};
        for (int i= 0; i<list.length; i++) {
            aTree.addElement(list[i]);
        }

        System.out.println("输出构造好的大顶堆序列（层序）" + aTree.toString());

        System.out.println("输出每轮排序的结果");
        for (int i = 0; i<list.length;i++) {
            list[i] = (int) aTree.removeMax();
            System.out.println("第" + (i+1) + "次： " + aTree);
        }

        System.out.println("排序结果：");
        for (int i= 0;i<list.length;i++)
            System.out.print(list[i]+" ");
    }


}
