package wek8;

public class ArrayHeapTest {
    public static void main(String[] args) {
        ArrayHeap arrayHeap = new ArrayHeap();
        arrayHeap.addElement("2");
        arrayHeap.addElement("8");
        arrayHeap.addElement("7");
        arrayHeap.addElement("1");
        arrayHeap.addElement("4");
        arrayHeap.addElement("0");
        arrayHeap.addElement("10");

        System.out.println("堆的生成为：");
        System.out.println(arrayHeap.toString() );

        System.out.println("删除最小元素后的为：");
        arrayHeap.removeMin() ;
        System.out.println(arrayHeap.toString());

        System.out.println("找到的最小值的元素为：");
        System.out.println(arrayHeap.findMin()) ;



    }
}
