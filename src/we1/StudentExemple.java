package we1;

import java.util.Scanner;

public class StudentExemple {
    //（1）通过键盘输入一些整数，建立一个链表。
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入一个整数:");
        int num1 = scan.nextInt();
        Student Head = new Student(num1);
        String reply = "";
        System.out.println("是否还要继续输入？y/n");
        reply = scan.next();
        if (reply.equalsIgnoreCase("y")) {
            do {
                System.out.println("请继续输入一个整数:");
                int num2 = scan.nextInt();
                System.out.println("是否还要继续输入？y/n");
                reply = scan.next();
                Student linkedlist = new Student(num2);
                InsertNode(Head, linkedlist);
            }
            while (reply.equalsIgnoreCase("y"));
        }
        System.out.println("用户输入的整数链表：");
        PrintLinkedlist(Head);
        System.out.println();
        System.out.println();
        System.out.println("请输入一个需要插入的数字： ");
        Scanner qq = new Scanner(System.in);
        int num = qq.nextInt();
        Student temp= new Student(num);
        System.out.println("请输入需要插入数字的位置：");
        Scanner qw =new Scanner(System.in);
        int n =qw.nextInt();
        Student temp1 =Head;
        for (int ii =1;ii<n;ii++){
            temp1=temp1.next;
        }
        InsertNode(Head,temp1,temp1.next,temp ) ;
        System.out.println("插入后的链表为：");
        PrintLinkedlist(Head) ;
        System.out.println();
        System.out.println("请输入想要删除的数字：");
        Scanner t = new Scanner(System.in);
        int nn = t.nextInt();
        Student t1 = new Student(nn);
        Deletenode(Head,t1) ;
        System.out.println("删除后的链表为：");
        PrintLinkedlist(Head) ;
        System.out.println();

        System.out.println("排序后的整数链表为：");
        selectionSort(Head) ;
        PrintLinkedlist(Head);
        System.out.println();
    }


    public static void PrintLinkedlist(Student Head) {
        Student node = Head;
        while (node != null) {
            System.out.print(" " + node.number);
            node = node.next;
        }
    }

    public static void InsertNode1(Student Head, Student node) {
        node.next = Head;
        Head = node;
        System.out.println("" + Head.number);
    }


    public static void InsertNode(Student Head, Student node) {
        Student temp = Head;

        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = node;
    }


    public static void InsertNode(Student Head, Student node1, Student node2,Student node3) {
        Student point = Head;
        while ((point.number != node1.number) && point != null) {
            point = point.next;
        }
        if (point.number == node1.number) {
            node3.next = point.next;
            point.next = node3;
        }
    }

    //链表的删除方法。
    public static void Deletenode(Student Head, Student node) {
        Student prenode = Head, currentnode = Head;
        while (prenode != null) {
            if (currentnode.number  != node.number ) {
                prenode = currentnode;
                currentnode = currentnode.next;
            } else {
                break;
            }
        }
        prenode.next = currentnode.next;
    }

    //链表的选择排序法。
    public static Student selectionSort(Student Head) {
        //记录每次循环的最小值
        int temp;
        Student curNode = Head;
        while (curNode != null) {
            /**
             * 内重循环从当前节点的下一个节点循环到尾节点，
             * 找到和外重循环的值比较最小的那个，然后与外重循环进行交换
             */
            Student nextNode = curNode.next;
            while (nextNode != null) {
                //比外重循环的小值放到前面
                if (nextNode.number > curNode.number) {
                    temp = nextNode.number;
                    nextNode.number = curNode.number;
                    curNode.number = temp;
                }
                nextNode = nextNode.next;
            }
            curNode = curNode.next;
        }
        return Head;
    }
}
