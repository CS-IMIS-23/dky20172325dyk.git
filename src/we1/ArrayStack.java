package we1;

import java.util.Arrays;

public class ArrayStack<T> implements StackADT<T>{
    private final int DEFAULT_CAPACITY = 100;
    private int top;
    private T[] stack;

    public ArrayStack() {
        top = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public ArrayStack(int initialCapacity)
    {
        top = 0;
        stack = (T[])(new Object[initialCapacity]);
    }


    @Override
    public T pop() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }
        top--;
        T result = stack[top];
        stack[top] = null;

        return result;
    }


    @Override
    public  int size() {//确定栈的元素数目
        return top;

    }

    @Override
    public boolean isEmpty() {//确定栈是否为空
        if (top == 0) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public T peek() throws EmptyCollectionException {//查看栈顶部的元素
        if (isEmpty()) {
            throw new EmptyCollectionException("Stack");
        }

        return stack[top - 1];
    }
    @Override
    public void push(T element) {//添加一个元素到栈的顶部
        if (size() == stack.length) {
            expandCapacity();
        }
        stack[top] = element;
        top++;
    }

    private void expandCapacity() {
        stack = Arrays.copyOf(stack, stack.length * 2);
    }

    @Override
    public String toString() {
        String result = "";
        for (int i = 0; i < size() ; i++) {
            result += stack[i]+" ";
        }
        return result;

    }
}
