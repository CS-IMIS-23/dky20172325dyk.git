/*
    PP7.4      Author: Dyk

    Homework
*/
public class RationalNumber extends Object implements Comparable
{
    private int numerator, denominator;
    public RationalNumber(int numer, int denom)
    {
        if (denom == 0)
            denom = 1;

        if (denom < 0)
        {
            numer = numer * -1;
            denom = denom * -1;
        }

        numerator = numer;
        denominator = denom;
        reduce();
    }
    public int compareTo(Object obj)
    {
        RationalNumber a =(RationalNumber)obj;
        double num1, num2, cha;
        num1 = 1.0 * this.numerator / this.denominator;
        num2 = 1.0 * a.getNumerator() / a.getDenominator();
        cha = num1 - num2;

        if(cha > 0.0001)
            return 1;
        else if((cha < 0.0001) & (cha != 0))
            return -1;
        else
            return 0;
    }
    public int getNumerator()
    {
        return numerator;
    }

    public int getDenominator()
    {
        return denominator;
    }

    public RationalNumber reciprocal()
    {
        return new RationalNumber(denominator, numerator);
    }


    public RationalNumber add(RationalNumber op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int sum = numerator1 + numerator2;

        return new RationalNumber(sum, commonDenominator);
    }

    public RationalNumber subtract(RationalNumber op2)
    {
        int commonDenominator = denominator * op2.getDenominator();
        int numerator1 = numerator * op2.getDenominator();
        int numerator2 = op2.getNumerator() * denominator;
        int difference = numerator1 - numerator2;

        return new RationalNumber(difference, commonDenominator);
    }

    public RationalNumber multiply(RationalNumber op2)
    {
        int numer = numerator * op2.getNumerator();
        int denom = denominator * op2.getDenominator();

        return new RationalNumber(numer,denom);
    }

    public RationalNumber divide(RationalNumber op2)
    {
        return multiply(op2.reciprocal());
    }

    public boolean isLike(RationalNumber op2)
    {
        return ( numerator == op2.getNumerator() &&
                  denominator == op2.getDenominator() );
    }

    public String toString()
    {
        String result;
        if (numerator == 0)
            result = "0";
        else
            result = numerator + "/" + denominator;
        return result;
    }

    private void reduce()
    {
        if (numerator != 0)
        {
            int common = gcd(Math.abs(numerator), denominator);

            numerator = numerator / common;
            denominator = denominator / common;
        }
    }

    private int gcd(int num1, int num2)
    {
        while (num1 != num2)
            if(num1 > num2)
                num1 = num1 - num1;

        return num1;
    }
}
