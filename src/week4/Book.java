public class Book
{
   String bookname;

   String author;
 
   String press;

   String copyrightdate;

 public Book(String A,String B,String C,String D)

  {
    bookname = A;
    author = B;
    press = C;
    copyrightdate = D;
  }

public String getbookname()
 { 
  return bookname;
 } 
  public void setbookname(String bookname)
   {
    bookname = bookname;
   }
public String getauthor()
 { 
  return author;
 }
  public void setauthor(String author)
   {
    author = author;
   }
public String getpress()
 { 
  return press;
 }
  public void setpress(String press)
   {
    press = press;
   }
public String getcopurightdate()
 { 
  return copyrightdate;
 }
  public void setcopyrightdate(String copyrightdate)
   {
    copyrightdate = copyrightdate;
   }


public String toString()
  { 
   return "\nBookname: "+bookname+"\nAuthor: "+author+"\npress: "+press+"\nCopyrightdate: "+copyrightdate;
  }



}
