//--------------------------------------------------------------
// classpractice.java            Author: Dyk
//
//  A homework 
//--------------------------------------------------------------
import java.util.Random;
import java.lang.Integer;

public class classpractice
{
    public static void main(String[] args)
    {
        Random generator = new Random();

        int PseudoNumber;
        String  num2, num3;

        PseudoNumber = generator.nextInt(20) - 10;
        System.out.println("生成的随机数为: " + PseudoNumber);

        num2 = Integer.toBinaryString(PseudoNumber);
        System.out.println("该数由二进制表示为: " + num2);
        
        num3 = Integer.toHexString(PseudoNumber);
        System.out.println("该数由十六进制表示为: " + num3);
    }
}


