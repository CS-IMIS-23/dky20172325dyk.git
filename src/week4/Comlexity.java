//--------------------------------------------------------------------
//    Complexity.java     Author:Dyk
//
//    Represents the interface for an object that can be assigned an
//    explicit compl
//---------------------------------------------------------------------

 interface Complexity
{

  public void setComplexity(int complexity);

  public int getComplexity();

}
