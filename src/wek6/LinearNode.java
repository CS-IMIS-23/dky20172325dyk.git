package wek6;

public class LinearNode {//因为这个实验最开始想的就是用整型值，所以就直接用整型了
    private LinearNode next;
    private int element;

    //creats an empty node
    public LinearNode() {
        next = null;
        element = Integer.parseInt(null);
    }

    //creats a node storing the specified element.
    public LinearNode(int elem) {
        next = null;
        element = elem;
    }

    //returns the node that follows this one
    public LinearNode getNext() {
        return next;
    }

    //Sets the node that follows this one
    public void setNext(LinearNode node) {
        next = node;
    }

}
