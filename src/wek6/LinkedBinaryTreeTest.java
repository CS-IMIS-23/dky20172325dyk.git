package wek6;

import java.util.Iterator;

public class LinkedBinaryTreeTest {
    public static void main(String[] args) {


        LinkedBinaryTree node4 = new LinkedBinaryTree(4);
        LinkedBinaryTree node5 = new LinkedBinaryTree(5);
        LinkedBinaryTree node2 = new LinkedBinaryTree(2, node5, node4);

        LinkedBinaryTree node8 = new LinkedBinaryTree(8);
        LinkedBinaryTree node9 = new LinkedBinaryTree(9);

        LinkedBinaryTree node6 = new LinkedBinaryTree(6, node8, node9);
        LinkedBinaryTree node7 = new LinkedBinaryTree(7);
        LinkedBinaryTree node3 = new LinkedBinaryTree(3, node7, node6);


        LinkedBinaryTree node1 = new LinkedBinaryTree(1, node2, node3);

        Iterator it;

        BinaryTreeNode s = new BinaryTreeNode(node2, node4, null);
        System.out.println("结点3是否是叶子结点：" + s.judge());
        BinaryTreeNode d = new BinaryTreeNode(node8);
        System.out.println("结点7是否是叶子结点：" + d.judge());


        System.out.println("树的大小是： " + node1.size());
        System.out.println("树为空吗？： " + node1.isEmpty());


        System.out.println("前序遍历结果为： ");
        it = node1.iteratorPreOrder();
        while (it.hasNext())
            System.out.print(it.next() + " ");

        System.out.println("中序遍历结果为： ");
        it = node1.iteratorInOrder();
        while (it.hasNext())
            System.out.print(it.next() + " ");


        System.out.println("后序遍历结果为： ");
        it = node1.iteratorPostOrder();
        while (it.hasNext())
            System.out.print(it.next() + " ");

        System.out.println("层序遍历结果为： ");
        it = node1.iteratorLevelOrder();
        while (it.hasNext())
            System.out.print(it.next() + " ");

        System.out.println("树为：");
        System.out.println(node1.printTree());

        node1.removeRightSubtree();
        System.out.println("删除右子树后输出树： ");
        System.out.println(node1.printTree());


        node1.removeAllElements();
        System.out.print("删除所有元素后输出树： ");
        System.out.println(node1.printTree());
    }

}