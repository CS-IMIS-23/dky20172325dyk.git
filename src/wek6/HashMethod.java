package wek6;

public class HashMethod {
    LinearNode[] hash;//所需的数组
    int conflicttime;//冲突的次数
    int time;//查找的总次数

    public HashMethod(int l) {
        hash = new LinearNode[l];
    }

    public void solve(int[] m) {
        for (int i = 0; i < m.length; i++) {//遍历
            LinearNode node = new LinearNode(m[i]);
            int p = m[i] % 11;//索引值
            LinearNode linearNode = hash[p];

            if (linearNode == null) {//如果这个地方是空的，就把数值直接放进去，然后查找次数增加
                hash[p] = node;
                time++;
            } else {//如果不是空的，这个时候就发生了冲突，就要用链地址法将其链接到相应的位置。
                LinearNode cur = hash[p];//先将已经是的那个作为开始
                while (cur.getNext() != null) {//当后面那个不是空，也就是不断的遍历然后找空的地方
                    cur = cur.getNext();
                    time++;
                }
                cur.setNext(node);//当找到了那个空的地方，就将那个冲突放进去
                conflicttime++;//冲突次数增加
                time++;//平均总的次数也增加
            }
        }
    }

    public static void main(String[] args) {
        int[] hash = {11, 78, 10, 1, 3, 2, 4, 21, 27};//06+21=27
        HashMethod hashMethod = new HashMethod(11);
        hashMethod.solve(hash);
        System.out.println("冲突的次数：" + hashMethod.conflicttime+"次");
        System.out.println("ASL的值为："+(hashMethod.time+hashMethod.conflicttime)+"/"+hash.length);

    }
}
