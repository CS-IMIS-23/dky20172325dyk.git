package wek6;

import java.io.FileNotFoundException;

/**
 * @author LbZhang
 * @version 创建时间：2015年11月23日 下午5:13:43
 * @description 背部疼痛诊断器
 *
 */
public class BackPainAnalyzer {

    /**
     * Asks questions of the user to diagnose a medical problem.
     */
    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("So, you're having back  .");

        DecisionTree expert = new DecisionTree("input.txt");
        expert.evaluate();
    }

}