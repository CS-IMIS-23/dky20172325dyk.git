package wek6;

import wek1.LinearNode;

public class Haxiexam {
    int[] myarray;
    int A;
    int chongtu = 0;
    int times = 0;


    Haxiexam(int[] a) {
        A = a.length;
        myarray = a;
        linearnode = new LinearNode[11];
    }

    LinearNode[] linearnode;
    public LinearNode[] create() {
        for (int a = 0; a < A; a++) {
            LinearNode node = new LinearNode();

            node.setElement (myarray[a]);
            int mulu = myarray[a] % 11;

            if (linearnode[mulu] == null) {
                linearnode[mulu] = node;
            }
            else {
                LinearNode newnode = linearnode[mulu];
                while (newnode.getNext() != null) {
                    newnode = newnode.getNext();
                }
                newnode.setNext(node);
            }
        }

        return linearnode;
    }


    public double getasl() {
        double asl = 0;
        for (int i = 0; i < linearnode.length; i++) {
            LinearNode newnode2 = linearnode[i];
            int num = 1;

            while (newnode2 != null) {
                if (num >= 2)
                    chongtu++;

                times = times + num;
                newnode2 = newnode2.getNext();
                num++;
            }
        }

        asl = (double) times / A;
        return asl;
    }

    public int getConflict(){
        return chongtu;
    }
}
