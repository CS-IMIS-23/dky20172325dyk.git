package wek6;

import wek1.EmptyCollectionException;

public class BinaryTreeNode<T> {

    protected T element;
    protected BinaryTreeNode<T> left;
    protected BinaryTreeNode<T> right;

    /**
     * Creates a new tree node with the specified data.
     *
     * @param obj
     *            the element that will become a part of the new tree node
     */
    public BinaryTreeNode(T obj) {
        this.element = obj;
        this.left = null;
        this.right = null;
    }


    ///合并构建声明
    public BinaryTreeNode(T obj, LinkedBinaryTree<T> left,
                          LinkedBinaryTree<T> right)throws EmptyCollectionException {
        element = obj;
        if (left == null)
            this.left = null;
        else
            this.left = left.getRootNode();

        if (right == null)
            this.right = null;
        else
            this.right = right.getRootNode();
    }

    /**
     * Returns the number of non-null children of this node.
     * @return the integer number of non-null children of this node
     * 使用递归的方式
     */
    public int numChildren() {
        int children = 0;

        if (left != null)
            children = 1 + left.numChildren();

        if (right != null)
            children = children + 1 + right.numChildren();

        return children;
    }

    /**
     * Return the element at this node.
     * @return the element stored at this node
     */
    public T getElement() {
        return element;
    }

    public BinaryTreeNode<T> getRight() {
        return right;
    }

    public void setRight(BinaryTreeNode<T> node) {
        right = node;
    }

    public BinaryTreeNode<T> getLeft() {
        return left;
    }

    public void setLeft(BinaryTreeNode<T> node) {
        left = node;
    }

    public boolean judge(){
        if(right == null && left == null)
            //叶结点的左侧和右侧都没有结点
            return true;
        else
            return false;
    }

}