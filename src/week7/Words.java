/*
      Words.java         Author: Dyk

      Demonstrates the use of an inherited method.
     */


import java.util.Dictionary;

public class Words {
    //------------------------------------------------
    //Instantiates a derived class and invokes its inherited and
    //local methods.
    //--------------------------------------------------

    public static void main(String[] args) {
        Dictionary webster = new Dictionary();

        System.out.println("NUmber of pages: " + webster.getPages());

        System.out.println("NUmber of definitions: " + webster.getDefinitions());

        System.out.println("NUmber of per page: " + webster.computeRatio());

        }
    }
