/* 
     Book2.java                       Author: Dyk
  
     Represents a book.Used as the parent of a derived class to
     demonstrates inheritance and the use of the super reference.

*/

public class Book2
{
   protected int pages ;

   //
   //Construtor:Sets up the book with the specified number of
   //pages.
   //
   public Book2(int numPages)
   {
     pages = numPages;
   }

   //
   // Pages mutator.
   //
   public void setPages(int numPages)
   {
     pages = numPages;
   }

   //
   // Pages accessor.
   //
   public int getPages()
   {
     return pages;
   }

}

