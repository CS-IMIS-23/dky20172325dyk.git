package week7;

public abstract class Animal {
     String name;
     int id;

    public Animal(String name, int id) {
        this.name = name;
        this.id = id;
}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public abstract void eat();

    public abstract void sleep();

    public abstract void introduction() ;
}
