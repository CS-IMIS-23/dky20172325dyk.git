package week7;

import javax.naming.Name;

public class Cow extends Animal{
    public Cow(String name, int id) {
        super(name, id);
    }

    @Override
    public void eat() {
        System.out.println("那牛" + getName() + "在吃草" );
    }

    @Override
    public void sleep() {
         System .out.println("那牛" + getName() + "在睡觉" );
    }

    @Override
    public void introduction() {
        System.out.println("大家好，我叫" + getName() + "，我的ID是" + getId());
    }
}
