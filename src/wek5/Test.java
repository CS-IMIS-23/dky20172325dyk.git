package wek5;

public class Test {
    public static void main(String[] args) {

        String list[] = {"apple","banana","orange","peach","grapes"};

        Integer list1[] = {2,35,6,7,8,44,78,43,88,1,23};

        Double list2[] = {1.2, 2.3, 3.4, 4.5, 5.6, 6.7, 7.8, 8.9};

        System.out.println("使用相同的列表list1");

        System.out.println("使用选择排序：");
        NewSorting.selectionSort(list1);
        System.out.println();

        System.out.println("使用插入排序：");
        NewSorting.insertionSort(list1);
        System.out.println();

        System.out.println("使用冒泡排序：");
        NewSorting.bubbleSort(list1);
        System.out.println();

        System.out.println("使用快速排序：");
        NewSorting.quickSort(list1);
        System.out.println();

        System.out.println("使用归并排序：");
        NewSorting.mergeSort(list1);
        System.out.println();

        for(int number:list1) {
            System.out.print(number + " ");
        }

        System.out.println();

        System.out.println("使用归并排序来对一个Double型列表进行排序：");
        NewSorting.mergeSort(list2);
        for(double number:list2)
            System.out.print(number+" ");
        System.out.println();

        System.out.println("使用快速排序来对一个String列表进行排序：");
        NewSorting.quickSort(list);
        for(String line:list)
            System.out.print(line+" ");

    }
}
