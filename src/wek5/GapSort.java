package wek5;

public class GapSort {
    public static <T extends Comparable<T>> void sort(T[] data, int num) {

        int position, scan;
        T temp;
        for (position = num; position >= 1; position--) {//进行遍历
            scan = 0;
            while (scan < data.length - position) {//进行间隔为num的两个数的比较
                if (data[scan].compareTo(data[scan + num]) > 0) {
                    swap(data, scan, scan + num);
                }
                scan++;
            }
            num--; //间隔数逐渐减少
        }
    }

    private static <T extends Comparable<T>>
    void swap(T[] data, int index1, int index2) {
        T temp = data[index1];
        data[index1] = data[index2];
        data[index2] = temp;
    }

    //测试
    public static void main(String[] args) {
        Contact[] friends = new Contact[7];

        friends[0] = new Contact("John", "Smith", "610-555-7384");
        friends[1] = new Contact("Sarah", "Barnes", "215-555-3827");
        friends[2] = new Contact("Mark", "Riley", "733-555-2969");
        friends[3] = new Contact("Laura", "Getz", "663-555-3984");
        friends[4] = new Contact("Larry", "Smith", "464-555-3489");
        friends[5] = new Contact("Frank", "Phelps", "322-555-2284");
        friends[6] = new Contact("Marsha", "Grant", "243-555-2837");

        GapSort.sort(friends, 4);

        for (Contact friend : friends)
            System.out.println(friend);
    }
}