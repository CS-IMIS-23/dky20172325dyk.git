/*
    Family.java       Author: Dyk

    Demonstrates the use of variable length parameter lists

*/

public class Family
{
  private String[] members;

  //--------------------------------------------------------------
  //   Constructor: Sets up this family by storing the (possbly
  //   multiple) names that are passed in as parameter.
  //--------------------------------------------------------------
    public Family(String ... names)
    {
       members = names;
    }   

    //-----------------------------------------------------------
    //  Return a String representtation of this family.
    //-----------------------------------------------------------
    public String toString()
    {
      String result = "";

      for (String name : members)

      result += name + "\n";

      return result;
    }

}
