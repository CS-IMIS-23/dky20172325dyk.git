/*
   PP8.1                Author: Dyk

   Homework
  
   Hepler: 张旭升小助教
*/

import java.util.Scanner;

public class PP8_1
{
  public static void main(String []args)
  {
     int[] number = new int[50];
	
        // 这里的表示无限循环。见课本147页，若结果为true则一直循环，当结果为false时停止，于是在后面的语句中加上了if语句
        // 用来执行后续操作。
        while(true)
        {
          Scanner scan = new Scanner(System.in);
	  System.out.println("输入一个0——50之间的整数：");
	  int a = scan.nextInt();
	  a--;
	  
        // 如果输入的值比0小或者比50大的话，就会自动的退出输入。这里的break语句是
        // 跳出循环的作用，目的是当输入的数值不满足数组要求时终止程序。
          if(a < 0 || a > 50)		
	 	 break;
	  number[a]++;
          
	}

        for(int i=0;i<number.length;i++)
        {
           if(number[i]!=0)
             {
                System.out.println(i+1+":" + number[i]+"\t");
             }
        }

        }
 }

