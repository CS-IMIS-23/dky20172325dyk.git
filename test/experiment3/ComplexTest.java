package experiment3;

import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a = new Complex(2, 5);
    Complex b = new Complex(3, -4);


    @Test
    public void testComplexAdd()throws Exception {
        assertEquals("5.0+1.0i", a.ComplexAdd(b).toString());
    }
    @Test
    public void testComplexSub()throws Exception {
        assertEquals("-1.0+9.0i",a.ComplexSub(b ).toString() );
    }
    @Test
    public void testComplexMulti()throws Exception {
        assertEquals("26.0+7.0i",a .ComplexMulti(b ).toString() );
    }
    @Test
    public void testComplexDiv()throws Exception {
        assertEquals("-2.0-3.0i",a .ComplexDiv(b ).toString() );
    }
}

